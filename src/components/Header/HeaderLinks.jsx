import React from "react";
import classNames from "classnames";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Hidden from "@material-ui/core/Hidden";
import Poppers from "@material-ui/core/Popper";
// @material-ui/icons
import Icon from "@material-ui/core/Icon";
import Person from "@material-ui/icons/Person";
import Notifications from "@material-ui/icons/Notifications";
// import Dashboard from "@material-ui/icons/Dashboard";
// import Search from "@material-ui/icons/Search";
// core components
// import CustomInput from "components/CustomInput/CustomInput.jsx";
import Button from "components/CustomButtons/Button.jsx";

import headerLinksStyle from "assets/jss/material-dashboard-react/components/headerLinksStyle.jsx";

class HeaderLinks extends React.Component {
  state = {
    open: false,
    openprofile: false
  };
  handleToggle = () => {
    this.setState(state => ({ open: !state.open }));
  };

  handleTogglepProfile = () => {
    this.setState(state => ({ openprofile: !state.openprofile }));
  };

  handleClose = event => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }
    this.setState({ open: false });
  };

  handleCloseProfile = event => {
    if (this.anchorEl2.contains(event.target)) {
      return;
    }
    this.setState({ openprofile: false });
  };
  logout = event => {
    if (this.anchorEl2.contains(event.target)) {
      return;
    }
    this.setState({ openprofile: false });
    localStorage.clear("token");
    window.location.assign("/");
  };

  render() {
    const { classes } = this.props;
    const { open, openprofile } = this.state;
    return (
      <div>
        <div className={classes.manager}>
          <Button
            buttonRef={node => {
              this.anchorEl = node;
            }}
            color={window.innerWidth > 959 ? "transparent" : "white"}
            justIcon={window.innerWidth > 959}
            simple={!(window.innerWidth > 959)}
            aria-owns={openprofile ? "menu-list-grow" : null}
            aria-haspopup="true"
            onClick={this.handleToggle}
            className={classes.buttonLink}
            aria-label="Person"
          >
            <Notifications className={classes.icons} />
            <span className={classes.notifications}>5</span>
            <Hidden mdUp implementation="css">
              <p className={classes.linkText}>Notification</p>
            </Hidden>
          </Button>
          <Poppers
            open={open}
            anchorEl={this.anchorEl}
            transition
            disablePortal
            className={
              classNames({ [classes.popperClose]: !open }) +
              " " +
              classes.pooperNav
            }
          >
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                id="menu-list-grow"
                style={{
                  transformOrigin:
                    placement === "top" ? "left top" : "left bottom"
                }}
              >
                <Paper>
                  <ClickAwayListener onClickAway={this.handleClose}>
                    <MenuList role="menu">
                      <MenuItem
                        onClick={this.handleClose}
                        className={classes.dropdownItem}
                      >
                        <div className="mt-auto mb-auto">
                          <Icon
                            style={{
                              paddingTop: "2px",
                              marginRight: "4px",
                              color: "red"
                            }}
                          >
                            error
                          </Icon>
                          <span>
                            Sale มีการเปลี่ยนสถานะให้กับบริษัท... เป็นสถานะ 1.2
                          </span>
                        </div>
                      </MenuItem>
                      <MenuItem
                        onClick={this.handleClose}
                        className={classes.dropdownItem}
                      >
                        <div className="mt-auto mb-auto">
                          <Icon
                            style={{
                              paddingTop: "2px",
                              marginRight: "4px",
                              color: "red"
                            }}
                          >
                            error
                          </Icon>
                          <span>
                            Sale มีการเปลี่ยนสถานะให้กับบริษัท... เป็นสถานะ 1.2
                          </span>
                        </div>
                      </MenuItem>
                    </MenuList>
                  </ClickAwayListener>
                </Paper>
              </Grow>
            )}
          </Poppers>
        </div>

        {/* icon person */}
        <div className={classes.manager}>
          <Button
            buttonRef={node => {
              this.anchorEl2 = node;
            }}
            color={window.innerWidth > 959 ? "transparent" : "white"}
            justIcon={window.innerWidth > 959}
            simple={!(window.innerWidth > 959)}
            aria-owns={openprofile ? "menu-list-grow" : null}
            aria-haspopup="true"
            onClick={this.handleTogglepProfile}
            className={classes.buttonLink}
            aria-label="Person"
          >
            <Person className={classes.icons} />
            <Hidden mdUp implementation="css">
              <p className={classes.linkText}>Profile</p>
            </Hidden>
          </Button>
          <Poppers
            open={openprofile}
            anchorEl={this.anchorEl2}
            transition
            disablePortal
            className={
              classNames({ [classes.popperClose]: !openprofile }) +
              " " +
              classes.pooperNav
            }
          >
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                id="menu-list-grow"
                style={{
                  transformOrigin:
                    placement === "bottom" ? "left top" : "left bottom"
                }}
              >
                <Paper>
                  <ClickAwayListener onClickAway={this.handleCloseProfile}>
                    <MenuList role="menu">
                      <a href="/userprofile">
                        <MenuItem
                          onClick={this.handleCloseProfile}
                          className={classes.dropdownItem}
                        >
                          Profile
                        </MenuItem>
                      </a>
                      <MenuItem
                        onClick={this.logout}
                        className={classes.dropdownItem}
                      >
                        Logout
                      </MenuItem>
                    </MenuList>
                  </ClickAwayListener>
                </Paper>
              </Grow>
            )}
          </Poppers>
        </div>
      </div>
    );
  }
}

export default withStyles(headerLinksStyle)(HeaderLinks);
