import React, { Component } from "react";
import "./util.css";
import "./main.css";
import "../../assets/fonts/Linearicons/icon-font.min.css";
import axios from "axios";
import { PulseLoader } from "react-spinners";
import "../../assets/css/load.css";
// import {Col, Row, NavLink} from 'reactstrap'
import logo from "../../assets/img/mks_logo.png";
class Login extends Component {
  state = {
    email: "",
    password: "",
    Loading: false
  };
  componentDidMount() {
    if (localStorage.getItem("token") !== null) {
      window.location.assign("/tables");
    }
  }
  constructor(props) {
    super(props);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.login = this.login.bind(this);
  }
  login() {
    this.setState({
      Loading: true
    });
    const params = {
      username: this.state.email,
      password: this.state.password
    };
    const url = "http://203.154.58.180:5000/login";
    axios
      .post(url, JSON.stringify(params), {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        if (res.data.token != null) {
          localStorage.setItem("token", res.data.token);
          localStorage.setItem("data", JSON.stringify(res.data.data));
          window.location.assign("/tables");
        } else {
          this.setState({
            Loading: false
          });
          alert("ไม่สามารถเข้าสู่ระบบได้");
        }
      })
      .catch(er => {
        this.setState({
          email: "",
          password: "",
          Loading: false
        });
        alert("ไม่สามารถเข้าสู่ระบบได้");
      });
  }

  handleEmailChange(event) {
    this.setState({ email: event.target.value });
  }
  handlePasswordChange(event) {
    this.setState({ password: event.target.value });
  }

  render() {
    return (
      <div className="container-login100">
        {this.state.Loading && (
          <PulseLoader sizeUnit={"px"} size={50} color={"#F48F0C"} />
        )}
        <div className="wrap-login100 p-l-50 p-t-45 p-r-50 p-b-30">
          <span className="login100-form-title p-b-25">
            <img
              src={logo}
              className="App-logo"
              alt="logo"
              height="160"
              width="85% p-b-10"
            />
          </span>

          <div className="wrap-input100 validate-input m-b-16">
            <input
              className="input100"
              type="text"
              id="email"
              placeholder="Username"
              onChange={this.handleEmailChange}
              value={this.state.email}
            />
            <span className="focus-input100" />
            <span className="symbol-input100">
              <span className="lnr lnr-user" />
            </span>
          </div>

          <div className="wrap-input100 validate-input m-b-16">
            <input
              className="input100"
              type="password"
              id="pass"
              placeholder="Password"
              onChange={this.handlePasswordChange}
              value={this.state.password}
            />
            <span className="focus-input100" />
            <span className="symbol-input100">
              <span className="lnr lnr-lock" />
            </span>
          </div>
          <div className="container-login100-form-btn p-t-25">
            <button className="login100-form-btn" onClick={this.login}>
              Login
            </button>
          </div>
          <hr
            style={{
              height: "12px",
              border: "0",
              boxShadow: "inset 0 12px 12px -12px rgba(0, 0, 0, 0.5)"
            }}
          />
          <div className="text-center w-full">
            <span className="txt1">
              สมัครสมาชิก?{" "}
              <a className="txt1 bo1 hov1" href="/register">
                Sign up now
              </a>
            </span>
          </div>
        </div>
      </div>
    );
  }
}
export default Login;
