import Dashboard from "layouts/Dashboard/Dashboard.jsx";
import Login from "../layouts/Login/Login.jsx";
// import TableList from "views/TableList/TableList.jsx";

const indexRoutes = [
  { path: "/login", name: "Login", component: Login },
  { path: "/dashboard", name: "Dashboard", component: Dashboard }
];

export default indexRoutes;
