// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
// import Person from "@material-ui/icons/Person";
// import ContentPaste from "@material-ui/icons/ContentPaste";
// import LibraryBooks from "@material-ui/icons/LibraryBooks";
// import BubbleChart from "@material-ui/icons/BubbleChart";
// import LocationOn from "@material-ui/icons/LocationOn";
// import Notifications from "@material-ui/icons/Notifications";
// import Unarchive from "@material-ui/icons/Unarchive";
// core components/views
import DashboardPage from "views/Dashboard/Dashboard.jsx";
// import UserProfile from "views/UserProfile/UserProfile.jsx";
import TableList from "views/TableList/TableList.jsx";
import Addcustomer from "../views/Addcustomer/Addcustomer.jsx";
import ApproveUser from "../views/ApproveUser/ApproveUser.jsx";
import Webview from "../views/WebView/Webview.jsx";
import UserProfile from "../views/UserProfile/UserProfile.jsx";
import RequestPermission from "../views/RequestPermission/RequestPermossion.jsx";
import Webview from "../views/WebView/Webview.jsx";
// import Login from "../layouts/Login/Login.jsx";
// import Register from "../views/Register/Register.jsx";
// import Typography from "views/Typography/Typography.jsx";
// import Icons from "views/Icons/Icons.jsx";
// import Maps from "views/Maps/Maps.jsx";
// import NotificationsPage from "views/Notifications/Notifications.jsx";
// import UpgradeToPro from "views/UpgradeToPro/UpgradeToPro.jsx";

const dashboardRoutes = [
  {
    path: "/dashboard",
    sidebarName: "หน้าแรก",
    navbarName: "หน้าแรก",
    icon: Dashboard,
    component: DashboardPage,
    show: true
  },
  {
    path: "/tables",
    sidebarName: "ตารางงาน",
    navbarName: "ตารางงาน",
    icon: "content_paste",
    component: TableList,
    show: true
  },
  {
    path: "/addcustomer",
    sidebarName: "เพิ่มข้อมูลลูกค้าใหม่",
    navbarName: "เพิ่มข้อมูลลูกค้าใหม่",
    icon: "group_add",
    component: Addcustomer,
    show: false
  },
  {
    path: "/approveuser",
    sidebarName: "อนุมัติบัญชีผู้ใช้งาน",
    navbarName: "อนุมัติบัญชีผู้ใช้งาน",
    icon: "group_add",
    component: ApproveUser,
    show: true
  },
  {
    path: "/webview",
    sidebarName: "Web View",
    navbarName: "Web View",
    icon: "done",
    component: Webview,
    show: false
  },
  {
    path: "/userprofile",
    sidebarName: "โปรไฟล์",
    navbarName: "โปรไฟล์",
    icon: "done",
    component: UserProfile,
    show: false
  },
  {
    path: "/requestpermission",
    sidebarName: "ขอสิทธิ์ผู้ใช้งาน",
    navbarName: "ขอสิทธิ์ผู้ใช้งาน",
    icon: "people",
    component: RequestPermission,
    show: true
  },
  {
    path: "/webview",
    sidebarName: "ขอสิทธิ์ผู้ใช้งาน",
    navbarName: "ขอสิทธิ์ผู้ใช้งาน",
    icon: "people",
    component: Webview,
    show: true
  }
];

export default dashboardRoutes;
