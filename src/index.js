import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";

import "assets/css/material-dashboard-react.css?v=1.5.0";
import "bootstrap/dist/css/bootstrap.min.css";
// import indexRoutes from "routes/index.jsx";
import Dashboard from "layouts/Dashboard/Dashboard.jsx";
import Login from "./layouts/Login/Login.jsx";
import Register from "./views/Register/Register.jsx";
import ApproveUser from "./views/ApproveUser/ApproveUser.jsx";
import Webview from "./views/WebView/Webview.jsx";
// import AddCustomer from "./views/Addcustomer/Addcustomer.jsx";
// import ApproveUser from "./views/ApproveUser/ApproveUser.jsx";
// import AddCustomer from "./views/Addcustomer/Addcustomer.jsx";
// import TableList from "./views/TableList/TableList.jsx";

const hist = createBrowserHistory();

ReactDOM.render(
  <Router history={hist}>
    {/* <Switch>
      {indexRoutes.map((prop, key) => {
        return <Route path={prop.path} component={prop.component} key={key} />;
      })}
    </Switch> */}
    <div>
      <Switch>
        <Route path="/" exact component={Login} />
        {/* <Redirect from="/" to="/tables" /> */}
        {/* <Route path="/tables" component={Dashboard} /> */}
      </Switch>
      <Switch>
        <Route path="/dashboard" exact component={Dashboard} />
      </Switch>
      <Switch>
        <Route path="/dashboard" />
        <Redirect from="/dashboard" to="/tables" />
        <Route path="/tables" component={Dashboard} />
      </Switch>
      <Switch>
        <Route path="/register" component={Register} />
      </Switch>
      <Switch>
        <Route path="/dashboard" />
        <Redirect from="/dashboard" to="/addcustomer" />
        <Route path="/addcustomer" component={Dashboard} />
      </Switch>
      <Switch>
        <Route path="/dashboard" />
        <Redirect from="/dashboard" to="/webview" />
        <Route path="/webview" component={Dashboard} />
      </Switch>
      <Switch>
        <Route path="/dashboard" />
        <Redirect from="/dashboard" to="/userprofile" />
        <Route path="/userprofile" component={Dashboard} />
      </Switch>
      <Switch>
        <Route path="/dashboard" />
        <Redirect from="/dashboard" to="/approveuser" />
        <Route path="/approveuser" component={Dashboard} />
      </Switch>
      <Switch>
        <Route path="/dashboard" />
        <Redirect from="/dashboard" to="/webview" />
        <Route path="/webview" component={Webview} />
      </Switch>
    </div>
  </Router>,
  document.getElementById("root")
);
