import React from "react";
import PropTypes from "prop-types";
// react plugin for creating charts
import ChartistGraph from "react-chartist";
// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
// import Store from "@material-ui/icons/Store";
// import Warning from "@material-ui/icons/Warning";
// import DateRange from "@material-ui/icons/DateRange";
// import LocalOffer from "@material-ui/icons/LocalOffer";
// import Update from "@material-ui/icons/Update";
// import ArrowUpward from "@material-ui/icons/ArrowUpward";
// import AccessTime from "@material-ui/icons/AccessTime";
// import Accessibility from "@material-ui/icons/Accessibility";
// import BugReport from "@material-ui/icons/BugReport";
// import Code from "@material-ui/icons/Code";
// import Cloud from "@material-ui/icons/Cloud";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
// import Table from "components/Table/Table.jsx";
// import Tasks from "components/Tasks/Tasks.jsx";
// import CustomTabs from "components/CustomTabs/CustomTabs.jsx";
// import Danger from "components/Typography/Danger.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardIcon from "components/Card/CardIcon.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import MaterialTable from "material-table";
// import { bugs, website, server } from "variables/general.jsx";

import {
  // dailySalesChart,
  // emailsSubscriptionChart,
  completedTasksChart
} from "variables/charts.jsx";

import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";
import "../Dashboard/dashboard.css";

class Dashboard extends React.Component {
  state = {
    value: 0
  };
  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };
  render() {
    const { classes } = this.props;
    return (
      <div>
        <GridContainer>
          <GridItem xs={12} sm={6} md={3}>
            <Card className="card-cursor">
              <CardHeader color="warning" stats icon>
                <CardIcon color="warning">
                  <Icon>people</Icon>
                </CardIcon>
                <p className={classes.cardCategory}>สถานะทั้งหมด</p>
                <h3 className={classes.cardTitle}>
                  1953 <small>ราย</small>
                </h3>
              </CardHeader>
              <CardFooter stats>
                <div className={classes.stats}>
                  <Icon>people</Icon>
                  จำนวนลูกทั้งหมด
                </div>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <a href="/tables">
              <Card className="card-cursor">
                <CardHeader color="success" stats icon>
                  <CardIcon color="success">
                    <Icon>person_add_disabled</Icon>
                  </CardIcon>
                  <p className={classes.cardCategory}>สถานะ 0</p>
                  <h3 className={classes.cardTitle}>
                    215 <small>ราย</small>
                  </h3>
                </CardHeader>
                <CardFooter stats>
                  <div className={classes.stats}>
                    <Icon>person_add_disabled</Icon>
                    สถานะ 0 - ยังไม่มีการติดต่อ
                  </div>
                </CardFooter>
              </Card>
            </a>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <a href="/tables">
              <Card className="card-cursor">
                <CardHeader color="danger" stats icon>
                  <CardIcon color="danger">
                    <Icon>chat_bubble</Icon>
                  </CardIcon>
                  <p className={classes.cardCategory}>สถานะ 1</p>
                  <h3 className={classes.cardTitle}>
                    1465 <small>ราย</small>
                  </h3>
                </CardHeader>
                <CardFooter stats>
                  <div className={classes.stats}>
                    <Icon>chat_bubble</Icon>
                    สถานะ 1 - ยังไม่สนใจ
                  </div>
                </CardFooter>
              </Card>
            </a>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <a href="/tables">
              <Card className="card-cursor">
                <CardHeader color="info" stats icon>
                  <CardIcon color="info">
                    <Icon>transfer_within_a_station</Icon>
                  </CardIcon>
                  <p className={classes.cardCategory}>สถานะ 2</p>
                  <h3 className={classes.cardTitle}>
                    82 <small>ราย</small>
                  </h3>
                </CardHeader>
                <CardFooter stats>
                  <div className={classes.stats}>
                    <Icon>transfer_within_a_station</Icon>
                    สถานะ 2 - เข้าพบลูกค้าแล้ว
                  </div>
                </CardFooter>
              </Card>
            </a>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <a href="/tables">
              <Card className="card-cursor">
                <CardHeader color="primary" stats icon>
                  <CardIcon color="primary">
                    <Icon>monetization_on</Icon>
                  </CardIcon>
                  <p className={classes.cardCategory}>สถานะ 3</p>
                  <h3 className={classes.cardTitle}>
                    68 <small>ราย</small>
                  </h3>
                </CardHeader>
                <CardFooter stats>
                  <div className={classes.stats}>
                    <Icon>monetization_on</Icon>
                    สถานะ 3 - เสนอราคา
                  </div>
                </CardFooter>
              </Card>
            </a>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <Card className="card-cursor">
              <a href="/tables">
                <CardHeader color="rose" stats icon>
                  <CardIcon color="rose">
                    <Icon>shopping_cart</Icon>
                  </CardIcon>
                  <p className={classes.cardCategory}>สถานะ 4</p>
                  <h3 className={classes.cardTitle}>
                    115 <small>ราย</small>
                  </h3>
                </CardHeader>
                <CardFooter stats>
                  <div className={classes.stats}>
                    <Icon>shopping_cart</Icon>
                    สถานะ 4 - เกิดการสั่งซื้อ
                  </div>
                </CardFooter>
              </a>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <Card className="card-cursor">
              <a href="/tables">
                <CardHeader color="warning" stats icon>
                  <CardIcon color="warning">
                    <Icon>cached</Icon>
                  </CardIcon>
                  <p className={classes.cardCategory}>สถานะ 5</p>
                  <h3 className={classes.cardTitle}>
                    8 <small>ราย</small>
                  </h3>
                </CardHeader>
                <CardFooter stats>
                  <div className={classes.stats}>
                    <Icon>cached</Icon>
                    สถานะ 5 - ซื้อซ้ำ
                  </div>
                </CardFooter>
              </a>
            </Card>
          </GridItem>
        </GridContainer>
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <Card chart>
              <CardHeader color="danger" style={{ height: "320px" }}>
                <h3 style={{ marginTop: "-5px" }}>ภาพรวมสถานะ</h3>
                <ChartistGraph
                  className="ct-chart"
                  data={completedTasksChart.data}
                  type="Line"
                  options={completedTasksChart.options}
                  listener={completedTasksChart.animation}
                  style={{ height: "320px" }}
                />
              </CardHeader>
              <CardBody>
                <h4 className={classes.cardTitle}>ภาพรวมจำนวนคนแต่ละสถานะ</h4>
                <p className={classes.cardCategory}>
                  กราฟแสดงจำนวนคนของสัปดาห์นั้นๆ ของแต่ละสถานะ
                </p>
              </CardBody>
            </Card>
          </GridItem>
        </GridContainer>
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <Card>
              <CardHeader color="warning">
                <h4 className={classes.cardTitleWhite}>ตารางแสดงภาพรวมสถานะ</h4>
                <p className={classes.cardCategoryWhite}>
                  ตารางแสดงจำนวนคนของสัปดาห์นั้นๆ ของแต่ละสถานะ
                </p>
              </CardHeader>
              <CardBody>
                <div className="mt-4">
                  <MaterialTable
                    columns={[
                      { title: "สัปดาห์", field: "week", defaultSort: "desc" },
                      {
                        title: "ปี",
                        field: "year"
                      },
                      {
                        title: "สถานะ 0",
                        field: "status0"
                      },
                      { title: "สถานะ 1", field: "status1" },
                      { title: "สถานะ 2", field: "status2" },
                      {
                        title: "สถานะ 3",
                        field: "status3"
                      },
                      {
                        title: "สถานะ 4",
                        field: "status4"
                      },
                      {
                        title: "สถานะ 5",
                        field: "status5"
                      },
                      {
                        title: "ทั้งหมด",
                        field: "allstatus"
                      }
                    ]}
                    data={[
                      {
                        week: "1",
                        year: "2019",
                        status0: "197",
                        status1: "1478",
                        status2: "91",
                        status3: "72",
                        status4: "115",
                        status5: "8",
                        allstatus: "1961"
                      },
                      {
                        week: "2",
                        year: "2019",
                        status0: "194",
                        status1: "1472",
                        status2: "93",
                        status3: "77",
                        status4: "117 ",
                        status5: "8",
                        allstatus: "1961"
                      },
                      {
                        week: "3",
                        year: "2019",
                        status0: "195",
                        status1: "1619",
                        status2: "96",
                        status3: "78",
                        status4: "118",
                        status5: "8",
                        allstatus: "2114"
                      },
                      {
                        week: "4",
                        year: "2019",
                        status0: "1",
                        status1: "1771",
                        status2: "106",
                        status3: "85",
                        status4: "124",
                        status5: "9",
                        allstatus: "2096"
                      }
                    ]}
                    title="สรุปภาพรวมสถานะ"
                  />
                </div>
                {/* <Table
                  className="table-responsive"
                  tableHeaderColor="warning"
                  tableHead={[
                    "สัปดาห์",
                    "ปี",
                    "สถานะ 0",
                    "สถานะ 1",
                    "สถานะ 2",
                    "สถานะ 3",
                    "สถานะ 4",
                    "สถานะ 5",
                    "ทั้งหมด"
                  ]}
                  tableData={[
                    [
                      "44",
                      "2018",
                      "2013",
                      "708",
                      "39",
                      "59",
                      "109",
                      "8",
                      "1926"
                    ],
                    [
                      "45",
                      "2018",
                      "914",
                      "803",
                      "39",
                      "59",
                      "110",
                      "8",
                      "1933"
                    ],
                    [
                      "46",
                      "2018",
                      "793",
                      "930",
                      "39",
                      "59",
                      "110",
                      "8",
                      "1939"
                    ],
                    [
                      "47",
                      "2018",
                      "717",
                      "1018",
                      "40",
                      "58",
                      "109",
                      "8",
                      "1950"
                    ],
                    [
                      "48",
                      "2018",
                      "641",
                      "1091",
                      "44",
                      "58",
                      "111",
                      "8",
                      "1953"
                    ]
                  ]}
                /> */}
              </CardBody>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Dashboard);
