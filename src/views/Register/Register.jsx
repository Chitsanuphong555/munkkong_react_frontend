import React, { Component } from "react";
import {
  Row,
  Col,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
  Nav,
  NavLink,
  TabContent,
  TabPane,
  Collapse
} from "reactstrap";
import "../Register/register.css";
import classnames from "classnames";
import Paper from "@material-ui/core/Paper";
import logo from "../../assets/img/mks_logo.png";

export default class Register extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: "1"
    };
    this.toggle1 = this.toggle1.bind(this);
  }

  toggle1() {
    this.setState({ collapse: !this.state.collapse });
  }

  toggle(tab) {
    this.setState({
      activeTab: tab
    });
  }

  render() {
    return (
      <div className="background-image">
        <div className="paper-style container mt-3 mb-3">
          <Paper elevation={5}>
            <div className="form-regis">
              <div style={{ padding: "15px", textDecoration: "underline" }}>
                <a href="/" role="button">
                  <h6>กลับหน้าหลัก</h6>
                </a>
              </div>
              <div className="row" style={{ justifyContent: "center" }}>
                <div className="col-xs-12 col-sm-10 col-md-5">
                  <img
                    src={logo}
                    className="responsive ml-auto mr-auto"
                    alt="logo"
                  />
                </div>
              </div>

              <div style={{ padding: "15px" }}>
                <Form>
                  <h3>ลงทะเบียน</h3>
                  <FormGroup>
                    <div>
                      <FormGroup>
                        <Label for="exampleFristname">ชื่อ</Label>
                        <Input
                          type="text"
                          name="frist_name"
                          id="firstname"
                          placeholder="ชื่อ"
                          className="required"
                        />
                      </FormGroup>
                      <FormGroup>
                        <Label for="exampleLastname">ชื่อ-สกุล</Label>
                        <Input
                          type="text"
                          name="last_name"
                          id="lastname"
                          placeholder="สกุล"
                        />
                      </FormGroup>
                      <FormGroup>
                        <Label for="examplePhone">เบอร์โทรศัพท์</Label>
                        <Input
                          type="text"
                          name="phone_number"
                          id="phone"
                          placeholder="0xxxxxxxxx"
                          pattern="[0-9]{3} [0-9]{7}"
                          className="phone validate"
                          maxLength="11"
                        />
                      </FormGroup>
                      <FormGroup>
                        <Label for="exampleEmail">อีเมล</Label>
                        <Input
                          type="text"
                          name="email_text"
                          id="email"
                          placeholder="example@Email"
                        />
                      </FormGroup>
                      <FormGroup>
                        <Label for="IDcard">บัตรประชาชน</Label>
                        <Input
                          type="text"
                          name="ID_card"
                          id="idcard"
                          placeholder="รหัสบัตรประชาชน 13 หลัก"
                          pattern="[0-9]{13}"
                          className="CardId validate"
                          maxLength="13"
                        />
                      </FormGroup>
                      <Nav tabs>
                        <Col xs="12" sm="12" md="6">
                          <NavLink
                            style={{ fontSize: "17px" }}
                            className={classnames({
                              active: this.state.activeTab === "1"
                            })}
                            onClick={() => {
                              this.toggle("1");
                            }}
                          >
                            ยังไม่มี One Id
                          </NavLink>
                        </Col>
                        <Col xs="12" sm="12" md="6">
                          <NavLink
                            style={{ fontSize: "17px" }}
                            className={classnames({
                              active: this.state.activeTab === "2"
                            })}
                            onClick={() => {
                              this.toggle("2");
                            }}
                          >
                            มี One id
                          </NavLink>
                        </Col>
                      </Nav>
                      <TabContent activeTab={this.state.activeTab}>
                        <TabPane tabId="1">
                          <Row>
                            <Col xs="12" sm="12" md="12">
                              <div
                                style={{ textAlign: "left", padding: "20px" }}
                              >
                                <FormGroup>
                                  <Label for="UsernameOneId">
                                    ชื่อผู้ใช้งาน
                                  </Label>
                                  <Input
                                    type="text"
                                    name="RegisterName_OneId"
                                    id="UsernameOneId"
                                    placeholder="ชื่อผู้ใช้งาน"
                                    className="required"
                                  />
                                </FormGroup>

                                <FormGroup>
                                  <Label for="passwordOneId">รหัสผ่าน</Label>
                                  <Input
                                    type="password"
                                    name="RegisterPassword_oneId"
                                    id="passwordOneId"
                                    placeholder="รหัสผ่าน"
                                    className="required"
                                  />
                                </FormGroup>

                                <FormGroup>
                                  <Label for="confirmPassword">
                                    ยืนยันรหัสผ่าน
                                  </Label>
                                  <Input
                                    type="password"
                                    name="confirm_Password"
                                    id="confirmPassword"
                                    placeholder="ยืนยันรหัสผ่าน"
                                    className="required"
                                  />
                                </FormGroup>
                              </div>
                            </Col>
                          </Row>
                        </TabPane>

                        <TabPane tabId="2">
                          <Row>
                            <Col xs="12" sm="12" md="12">
                              <div
                                style={{ textAlign: "left", padding: "20px" }}
                              >
                                <FormGroup>
                                  <Label for="Username">ชื่อผู้ใช้งาน</Label>
                                  <Input
                                    type="text"
                                    name="user_name_OneId"
                                    id="Username"
                                    placeholder="ชื่อผู้ใช้งาน"
                                    className="required"
                                  />
                                </FormGroup>

                                <FormGroup>
                                  <Label for="password">รหัสผ่าน</Label>
                                  <Input
                                    type="text"
                                    name="password_OneId"
                                    id="password"
                                    placeholder="รหัสผ่าน"
                                    className="required"
                                  />
                                </FormGroup>

                                <div
                                  className="row ml-auto mr-auto"
                                  style={{ justifyContent: "center" }}
                                >
                                  <div className="col-sm-12 text-center">
                                    <Button
                                      className="btn btn-success"
                                      onClick={this.toggle1}
                                    >
                                      ยืนยัน
                                    </Button>
                                    <Collapse isOpen={this.state.collapse}>
                                      <div className="text-left mt-3">
                                        <Row>
                                          <Col xs="12" sm="12" md="6">
                                            <FormGroup>
                                              <Label for="exampleFristname">
                                                ชื่อ
                                              </Label>
                                              <Input
                                                type="text"
                                                name="frist_name"
                                                id="exampleFristname"
                                                placeholder="ชื่อ"
                                                className="required"
                                              />
                                            </FormGroup>
                                          </Col>
                                          <Col xs="12" sm="12" md="6">
                                            <FormGroup>
                                              <Label for="exampleLastname">
                                                ชื่อ-สกุล
                                              </Label>
                                              <Input
                                                type="text"
                                                name="last_name"
                                                id="exampleLastname"
                                                placeholder="สกุล"
                                              />
                                            </FormGroup>
                                          </Col>
                                        </Row>
                                        <Row>
                                          <Col xs="12" sm="12" md="6">
                                            <FormGroup>
                                              <Label for="examplePhone">
                                                เบอร์โทรศัพท์
                                              </Label>
                                              <Input
                                                type="text"
                                                name="phone_number"
                                                id="examplePhone"
                                                placeholder="0xxxxxxxxx"
                                                pattern="[0-9]{3} [0-9]{7}"
                                                className="phone validate"
                                                maxLength="11"
                                              />
                                            </FormGroup>
                                          </Col>
                                          <Col xs="12" sm="12" md="6">
                                            <FormGroup>
                                              <Label for="exampleEmail">
                                                อีเมล
                                              </Label>
                                              <Input
                                                type="text"
                                                name="email_text"
                                                id="exampleEmail"
                                                placeholder="example@Email"
                                              />
                                            </FormGroup>
                                          </Col>
                                        </Row>
                                        <Row>
                                          <Col xs="12" sm="12" md="6">
                                            <FormGroup>
                                              <Label for="SelectType">
                                                ประเภทผู้ใช้งาน
                                              </Label>
                                              <Input
                                                type="select"
                                                name="select"
                                                id="SelectType"
                                              >
                                                <option>ผู้ใช้งานทั่วไป</option>
                                                <option>ผู้เชี่ยวชาญ</option>
                                                <option>
                                                  หัวหน้าผู้เชี่ยวชาญ
                                                </option>
                                                <option>เจ้าหน้าที่</option>
                                              </Input>
                                            </FormGroup>
                                          </Col>
                                          <Col xs="12" sm="12" md="6">
                                            <FormGroup>
                                              <Label for="IDcard">
                                                บัตรประชาชน
                                              </Label>
                                              <Input
                                                type="text"
                                                name="ID_card"
                                                id="IDcard"
                                                placeholder="รหัสบัตรประชาชน 13 หลัก"
                                                pattern="[0-9]{13}"
                                                className="CardId validate"
                                                maxLength="13"
                                              />
                                            </FormGroup>
                                          </Col>
                                        </Row>
                                      </div>
                                    </Collapse>
                                  </div>
                                </div>
                              </div>
                            </Col>
                          </Row>
                        </TabPane>
                      </TabContent>
                    </div>
                  </FormGroup>

                  <div style={{ textAlign: "center" }}>
                    <FormGroup />
                    <FormGroup check>
                      {/* <Label>
                                                <Input type="checkbox" style={{ fontSize: "15px !important" }} />{' '}
                                                ยอมรับเงื่อนไข
                                            </Label> */}
                    </FormGroup>
                    <div
                      className="row ml-auto mr-auto"
                      style={{ justifyContent: "center" }}
                    >
                      <div className="col-sm-12">
                        <div className="mt-3">
                          <Button className="btn btn-success">ลงทะเบียน</Button>
                        </div>
                      </div>
                    </div>
                  </div>
                </Form>
              </div>
            </div>
          </Paper>
        </div>
      </div>
    );
  }
}
