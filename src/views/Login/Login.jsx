import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import "./util.css";
import "./main.css";
import "../../assets/fonts/Linearicons/icon-font.min.css";
import axios from "axios";
// import {Col, Row, NavLink} from 'reactstrap'
import logo from "../../assets/img/mks_logo.png";
class Login extends Component {
  state = {
    redirect: false
  };

  login() {
    const params = {
      username: "bezblue",
      password: "decade579"
    };
    const url = "http://203.154.58.180:5000/login";
    axios
      .post(url, JSON.stringify(params), {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        console.log(res);
      });
  }

  setRedirect = () => {
    this.setState({
      redirect: true
    });
  };

  renderRedirectHome = () => {
    if (this.state.redirect) {
      return <Redirect to="/dashboard" />;
    }
  };

  render() {
    return (
      <div className="container-login100">
        <div className="wrap-login100 p-l-50 p-t-45 p-r-50 p-b-30">
          <form action="/login">
            <span className="login100-form-title p-b-25">
              <img
                src={logo}
                className="App-logo"
                alt="logo"
                height="100"
                width="100% p-b-10"
              />
            </span>

            <div className="wrap-input100 validate-input m-b-16">
              <input
                className="input100"
                type="text"
                id="email"
                placeholder="Email"
              />
              <span className="focus-input100" />
              <span className="symbol-input100">
                <span className="lnr lnr-envelope" />
              </span>
            </div>

            <div className="wrap-input100 validate-input m-b-16">
              <input
                className="input100"
                type="password"
                id="pass"
                placeholder="Password"
              />
              <span className="focus-input100" />
              <span className="symbol-input100">
                <span className="lnr lnr-lock" />
              </span>
            </div>
            <div className="container-login100-form-btn p-t-25">
            
              <button className="login100-form-btn" onClick={this.login}>
                Login
              </button>
            </div>
            <hr
              style={{
                height: "12px",
                border: "0",
                boxShadow: "inset 0 12px 12px -12px rgba(0, 0, 0, 0.5)"
              }}
            />
            <div className="text-center w-full">
              <span className="txt1">
                สมัครสมาชิก?{" "}
                <a className="txt1 bo1 hov1" href="/register">
                  Sign up now
                </a>
              </span>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
export default Login;
