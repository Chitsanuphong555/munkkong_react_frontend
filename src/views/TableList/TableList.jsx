import React from "react";
import PropTypes from "prop-types";

import withStyles from "@material-ui/core/styles/withStyles";

import CustomInput from "components/CustomInput/CustomInput.jsx";
import Icon from "@material-ui/core/Icon";
import {
  Row,
  Col,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
  Modal,
  ModalHeader,
  FormText,
  ModalBody,
  ModalFooter
} from "reactstrap";

import Warning from "@material-ui/icons/Warning";

import PersonAddDisabled from "@material-ui/icons/PersonAddDisabled";
import Assessment from "@material-ui/icons/Assessment";
import AccountBox from "@material-ui/icons/AccountBox";
import Home from "@material-ui/icons/Home";
import EventNote from "@material-ui/icons/EventNote";
import Person from "@material-ui/icons/Person";
import ChatBubble from "@material-ui/icons/ChatBubble";
import TransferWithinAStation from "@material-ui/icons/TransferWithinAStation";
import MonetizationOn from "@material-ui/icons/MonetizationOn";
import ShoppingCart from "@material-ui/icons/ShoppingCart";
import Cached from "@material-ui/icons/Cached";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import CustomTabs from "components/CustomTabs/CustomTabs.jsx";
import MaterialTable from "material-table";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";

import conf from "../../conf.json";
import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";
import "../TableList/tablelist.css";
import axios from "axios";

import { PulseLoader } from "react-spinners";
import "../../assets/css/load.css";

class TableList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: 0,
      modal: false,
      modalUpload: false,
      modalExport: false,
      modalDeleteCompany: false,
      modalNotify: false,
      modalChangeStatus: false,
      modalReceiveEmail: false,
      data0: [],
      data1: [],
      data1_1: [],
      data1_2: [],
      data1_3: [],
      data1_4: [],
      data2: [],
      data3: [],
      data4: [],
      data5: [],
      tmp_contact: [],
      simpleSelect: "",
      selectall: "all",
      selectsale: "all",
      sales: [],
      Loading: false,
      notes: [],
      status: ""
    };

    this.toggledatatable = this.toggledatatable.bind(this);
    this.toggleReceiveEmail = this.toggleReceiveEmail.bind(this);
    this.toggleUpload = this.toggleUpload.bind(this);
    this.toggleExport = this.toggleExport.bind(this);
    this.toggleDeleteCompany = this.toggleDeleteCompany.bind(this);
    this.toggleNotify = this.toggleNotify.bind(this);
    this.toggleChangeStatus = this.toggleChangeStatus.bind(this);
    this.showContactModal = this.showContactModal.bind(this);
    this.getSales = this.getSales.bind(this);
    this.getContactMunkong = this.getContactMunkong.bind(this);
    this.handleChangeStatus = this.handleChangeStatus.bind(this);
  }

  componentDidMount() {
    this.getContactMunkong("all", "all");
    this.getSales();
  }

  getSales() {
    axios
      .get(conf.url + "/sale", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        let sales = res.data.data.map((data, index) => {
          return (
            <MenuItem key={index} value={data.acusername}>
              {data.acfirstname + " " + data.aclastname}
            </MenuItem>
          );
        });
        this.setState({
          sales: sales
        });
      });
  }

  getContactMunkong(sale, type) {
    this.setState({
      Loading: true
    });
    const params = {
      token: localStorage.getItem("token"),
      sale_id: sale,
      type: type
    };
    axios
      .post(conf.url + "/contact/get", JSON.stringify(params), {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        if (type === "all") {
          this.setState({
            data0: [],
            data1: [],
            data1_1: [],
            data1_2: [],
            data1_3: [],
            data1_4: [],
            data2: [],
            data3: [],
            data4: [],
            data5: []
          });
        } else {
          this.setState({
            data1_3: []
          });
        }

        let data0 = [];
        let data1 = [];
        let data1_1 = [];
        let data1_2 = [];
        let data1_3 = [];
        let data1_4 = [];
        let data2 = [];
        let data3 = [];
        let data4 = [];
        let data5 = [];
        res.data.data.map(data => {
          if (data.status === "0") {
            data0.push(data);
          } else if (
            data.status === "1" ||
            data.status === "1.1" ||
            data.status === "1.2" ||
            data.status === "1.3" ||
            data.status === "1.4"
          ) {
            data1.push(data);
            if (data.status === "1.1") {
              data1_1.push(data);
            } else if (data.status === "1.2") {
              data1_2.push(data);
            } else if (data.status === "1.3") {
              data1_3.push(data);
            } else if (data.status === "1.4") {
              data1_4.push(data);
            }
          } else if (data.status === "2") {
            data2.push(data);
          } else if (data.status === "3") {
            data3.push(data);
          } else if (data.status === "4") {
            data4.push(data);
          } else if (data.status === "5") {
            data5.push(data);
          }
        });
        if (type === "all") {
          this.setState({
            data0: data0,
            data1: data1,
            data1_1: data1_1,
            data1_2: data1_2,
            data1_3: data1_3,
            data1_4: data1_4,
            data2: data2,
            data3: data3,
            data4: data4,
            data5: data5
          });
        } else {
          this.setState({
            data1_3: data1_3
          });
        }
        this.setState({
          Loading: false
        });
      });
  }

  getNote(ct_id) {
    this.setState({
      Loading: true
    });
    const params = {
      token: localStorage.getItem("token")
    };
    axios
      .post(conf.url + "/note/" + ct_id, JSON.stringify(params), {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        this.setState({
          notes: res.data.data
        });
        this.setState({
          Loading: false
        });
      });
  }
  showContactModal(data) {
    this.setState({
      tmp_contact: data,
      status: data.status
    });
    this.toggledatatable();
    this.getNote(data.ct_running);
  }

  toggledatatable() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  toggleUpload() {
    this.setState(prevState => ({
      modalUpload: !prevState.modalUpload
    }));
  }

  toggleExport() {
    this.setState({
      modalExport: !this.state.modalExport
    });
  }

  toggleDeleteCompany() {
    this.setState({
      modalDeleteCompany: !this.state.modalDeleteCompany
    });
  }

  toggleNotify() {
    this.setState({
      modalNotify: !this.state.modalNotify
    });
  }

  toggleChangeStatus() {
    this.setState({
      modalChangeStatus: !this.state.modalChangeStatus
    });
  }

  toggleReceiveEmail() {
    this.setState(prevState => ({
      modalReceiveEmail: !prevState.modalReceiveEmail
    }));
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  handleChangeStatus = event => {
    this.setState({ status: event.target.value });
    this.setState({
      modalChangeStatus: !this.state.modalChangeStatus
    });
  };

  handleSelectSale = event => {
    this.setState({
      [event.target.name]: event.target.value,
      selectall: "all"
    });

    this.getContactMunkong(event.target.value, "all");
  };

  handleSelectSale_1_3 = event => {
    this.setState({ [event.target.name]: event.target.value });
    this.getContactMunkong(event.target.value, this.state.selectsale);
  };

  handleSelectAll = event => {
    this.setState({ [event.target.name]: event.target.value });
    this.getContactMunkong(this.state.selectsale, event.target.value);
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        {this.state.Loading && (
          <PulseLoader sizeUnit={"px"} size={50} color={"#F48F0C"} />
        )}
        <Row className="mr-1">
          <Col className="col-xs-12 text-right">
            <a href="/addcustomer">
              <Button
                color="success"
                style={{ color: "white", padding: "10px" }}
              >
                <Icon className="pt-1">add</Icon> เพิ่มข้อมูลลูกค้าใหม่{" "}
              </Button>{" "}
            </a>
            <Button
              color="info"
              style={{ color: "white", padding: "10px" }}
              onClick={this.toggleUpload}
            >
              <Icon className="pt-1">cloud_upload</Icon> อัพโหลด{" "}
            </Button>{" "}
            <Button
              color="primary"
              style={{ color: "white", padding: "10px" }}
              onClick={this.toggleExport}
            >
              <Icon className="pt-1">exit_to_app</Icon> ส่งออก Excel{" "}
            </Button>{" "}
          </Col>
        </Row>
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <CustomTabs
              title="สถานะทั้งหมด:"
              headerColor="danger"
              tabs={[
                {
                  // สถานะ 0
                  tabName: "สถานะ 0",
                  tabIcon: PersonAddDisabled,
                  tabContent: (
                    <div className="mt-4">
                      <h4>จำนวนลูกค้าทั้งหมด {this.state.data0.length} ราย</h4>

                      <div className="mt-4">
                        <div className="row ml-auto mr-auto justify-content-end">
                          <div className="col-xs-12 col-sm-6 col-md-4 mb-3">
                            <FormControl
                              fullWidth
                              className={classes.selectFormControl}
                            >
                              <InputLabel
                                htmlFor="simple-select"
                                className={classes.selectLabel}
                              >
                                เลือกฝ่ายขาย
                              </InputLabel>
                              <Select
                                MenuProps={{
                                  className: classes.selectMenu
                                }}
                                classes={{
                                  select: classes.select
                                }}
                                value={this.state.selectsale}
                                onChange={this.handleSelectSale}
                                inputProps={{
                                  name: "selectsale",
                                  id: "selectsale"
                                }}
                              >
                                <MenuItem key={-1} value={"all"}>
                                  {"ทั้งหมด"}
                                </MenuItem>
                                {this.state.sales}
                              </Select>
                            </FormControl>
                          </div>
                        </div>
                        <MaterialTable
                          title="สถานะ 0 - ยังไม่มีการติตต่อ"
                          onRowClick={(e, d) => this.showContactModal(d)}
                          columns={[
                            {
                              title: "สถานะ",
                              field: "status",
                              defaultSort: "desc"
                            },
                            { title: "ชื่อบริษัท", field: "company_thname" },
                            { title: "ชื่อลูกค้า", field: "contact_name" },
                            { title: "เบอร์ติดต่อ", field: "contact_no" },
                            { title: "อีเมล์", field: "contact_email" },
                            {
                              title: "ชื่อ Call Center",
                              field: "call_center_name"
                            },
                            { title: "ชื่อฝ่ายขาย", field: "sale_name" }
                          ]}
                          data={this.state.data0}
                        />
                      </div>
                    </div>
                  )
                },
                {
                  tabName: "สถานะ 1",
                  tabIcon: ChatBubble,
                  tabContent: (
                    <GridContainer>
                      <GridItem xs={12} sm={12} md={12}>
                        <CustomTabs
                          title="สถานะ 1:"
                          headerColor="info"
                          tabs={[
                            {
                              tabName: "สถานะ 1",
                              tabContent: (
                                <div className="mt-4">
                                  <h4>
                                    จำนวนลูกค้าทั้งหมด {this.state.data1.length}{" "}
                                    ราย
                                  </h4>
                                  <div className="row ml-auto mr-auto justify-content-end">
                                    <div className="col-xs-12 col-sm-6 col-md-4 mb-3">
                                      <FormControl
                                        fullWidth
                                        className={classes.selectFormControl}
                                      >
                                        <InputLabel
                                          htmlFor="selectsale"
                                          className={classes.selectLabel}
                                        >
                                          เลือกฝ่ายขาย
                                        </InputLabel>
                                        <Select
                                          MenuProps={{
                                            className: classes.selectMenu
                                          }}
                                          classes={{
                                            select: classes.select
                                          }}
                                          value={this.state.selectsale}
                                          onChange={this.handleSelectSale}
                                          inputProps={{
                                            name: "selectsale",
                                            id: "selectsale"
                                          }}
                                        >
                                          <MenuItem key={-1} value={"all"}>
                                            {"ทั้งหมด"}
                                          </MenuItem>
                                          {this.state.sales}
                                        </Select>
                                      </FormControl>
                                    </div>
                                  </div>
                                  <div className="mt-4">
                                    <MaterialTable
                                      onRowClick={(e, d) =>
                                        this.showContactModal(d)
                                      }
                                      columns={[
                                        {
                                          title: "สถานะ",
                                          field: "status",
                                          defaultSort: "desc"
                                        },
                                        {
                                          title: "ชื่อบริษัท",
                                          field: "company_thname"
                                        },
                                        {
                                          title: "ชื่อลูกค้า",
                                          field: "contact_name"
                                        },
                                        {
                                          title: "เบอร์ติดต่อ",
                                          field: "contact_no"
                                        },
                                        {
                                          title: "อีเมล์",
                                          field: "contact_email"
                                        },
                                        {
                                          title: "ชื่อ Call Center",
                                          field: "call_center_name"
                                        },
                                        {
                                          title: "ชื่อฝ่ายขาย",
                                          field: "sale_name"
                                        }
                                      ]}
                                      data={this.state.data1}
                                      title="สถานะ 1 - ยังไม่สนใจ"
                                    />
                                  </div>
                                </div>
                              )
                            },
                            {
                              tabName: "สถานะ 1.1",
                              tabContent: (
                                <div className="mt-4">
                                  <h4>
                                    จำนวนลูกค้าทั้งหมด{" "}
                                    {this.state.data1_1.length} ราย
                                  </h4>
                                  <div className="row ml-auto mr-auto justify-content-end">
                                    <div className="col-xs-12 col-sm-6 col-md-4 mb-3">
                                      <FormControl
                                        fullWidth
                                        className={classes.selectFormControl}
                                      >
                                        <InputLabel
                                          htmlFor="selectsale"
                                          className={classes.selectLabel}
                                        >
                                          เลือกฝ่ายขาย
                                        </InputLabel>
                                        <Select
                                          MenuProps={{
                                            className: classes.selectMenu
                                          }}
                                          classes={{
                                            select: classes.select
                                          }}
                                          value={this.state.selectsale}
                                          onChange={this.handleSelectSale}
                                          inputProps={{
                                            name: "selectsale",
                                            id: "selectsale"
                                          }}
                                        >
                                          <MenuItem key={-1} value={"all"}>
                                            {"ทั้งหมด"}
                                          </MenuItem>
                                          {this.state.sales}
                                        </Select>
                                      </FormControl>
                                    </div>
                                  </div>
                                  <div className="mt-4">
                                    <MaterialTable
                                      onRowClick={(e, d) =>
                                        this.showContactModal(d)
                                      }
                                      columns={[
                                        {
                                          title: "สถานะ",
                                          field: "status",
                                          defaultSort: "desc"
                                        },
                                        {
                                          title: "ชื่อบริษัท",
                                          field: "company_thname"
                                        },
                                        {
                                          title: "ชื่อลูกค้า",
                                          field: "contact_name"
                                        },
                                        {
                                          title: "เบอร์ติดต่อ",
                                          field: "contact_no"
                                        },
                                        {
                                          title: "อีเมล์",
                                          field: "contact_email"
                                        },
                                        {
                                          title: "ชื่อ Call Center",
                                          field: "call_center_name"
                                        },
                                        {
                                          title: "ชื่อฝ่ายขาย",
                                          field: "sale_name"
                                        }
                                      ]}
                                      data={this.state.data1_1}
                                      title="สถานะ 1.1 - ยังไม่สนใจ"
                                    />
                                  </div>
                                </div>
                              )
                            },
                            {
                              tabName: "สถานะ 1.2",
                              tabContent: (
                                <div className="mt-4">
                                  <h4>
                                    จำนวนลูกค้าทั้งหมด{" "}
                                    {this.state.data1_2.length} ราย
                                  </h4>
                                  <div className="row ml-auto mr-auto justify-content-end">
                                    <div className="col-xs-12 col-sm-6 col-md-4 mb-3">
                                      <FormControl
                                        fullWidth
                                        className={classes.selectFormControl}
                                      >
                                        <InputLabel
                                          htmlFor="selectsale"
                                          className={classes.selectLabel}
                                        >
                                          เลือกฝ่ายขาย
                                        </InputLabel>
                                        <Select
                                          MenuProps={{
                                            className: classes.selectMenu
                                          }}
                                          classes={{
                                            select: classes.select
                                          }}
                                          value={this.state.selectsale}
                                          onChange={this.handleSelectSale}
                                          inputProps={{
                                            name: "selectsale",
                                            id: "selectsale"
                                          }}
                                        >
                                          <MenuItem key={-1} value={"all"}>
                                            {"ทั้งหมด"}
                                          </MenuItem>
                                          {this.state.sales}
                                        </Select>
                                      </FormControl>
                                    </div>
                                  </div>
                                  <div className="mt-4">
                                    <MaterialTable
                                      onRowClick={(e, d) =>
                                        this.showContactModal(d)
                                      }
                                      columns={[
                                        {
                                          title: "สถานะ",
                                          field: "status",
                                          defaultSort: "desc"
                                        },
                                        {
                                          title: "ชื่อบริษัท",
                                          field: "company_thname"
                                        },
                                        {
                                          title: "ชื่อลูกค้า",
                                          field: "contact_name"
                                        },
                                        {
                                          title: "เบอร์ติดต่อ",
                                          field: "contact_no"
                                        },
                                        {
                                          title: "อีเมล์",
                                          field: "contact_email"
                                        },
                                        {
                                          title: "ชื่อ Call Center",
                                          field: "call_center_name"
                                        },
                                        {
                                          title: "ชื่อฝ่ายขาย",
                                          field: "sale_name"
                                        }
                                      ]}
                                      data={this.state.data1_2}
                                      title="สถานะ 1.2 - Call Center ติดตาม"
                                    />
                                  </div>
                                </div>
                              )
                            },
                            {
                              tabName: "สถานะ 1.3",
                              tabContent: (
                                <div className="mt-4">
                                  <h4>
                                    จำนวนลูกค้าทั้งหมด{" "}
                                    {this.state.data1_3.length} ราย
                                  </h4>
                                  <div className="row ml-auto mr-auto">
                                    <div className="col-xs-6 col-sm-6 col-md-4 mb-3">
                                      <FormControl
                                        fullWidth
                                        className={classes.selectFormControl}
                                      >
                                        <InputLabel
                                          htmlFor="selectall"
                                          className={classes.selectLabel}
                                        >
                                          สถานะการส่ง
                                        </InputLabel>
                                        <Select
                                          MenuProps={{
                                            className: classes.selectMenu
                                          }}
                                          classes={{
                                            select: classes.select
                                          }}
                                          value={this.state.selectall}
                                          onChange={this.handleSelectAll}
                                          inputProps={{
                                            name: "selectall",
                                            id: "selectall"
                                          }}
                                        >
                                          <MenuItem
                                            disabled
                                            classes={{
                                              root: classes.selectMenuItem
                                            }}
                                          >
                                            == สถานะการส่ง ==
                                          </MenuItem>
                                          <MenuItem
                                            classes={{
                                              root: classes.selectMenuItem,
                                              selected:
                                                classes.selectMenuItemSelected
                                            }}
                                            value="all"
                                          >
                                            ทั้งหมด
                                          </MenuItem>
                                          <MenuItem
                                            classes={{
                                              root: classes.selectMenuItem,
                                              selected:
                                                classes.selectMenuItemSelected
                                            }}
                                            value="Y"
                                          >
                                            ส่งแล้ว
                                          </MenuItem>
                                          <MenuItem
                                            classes={{
                                              root: classes.selectMenuItem,
                                              selected:
                                                classes.selectMenuItemSelected
                                            }}
                                            value="N"
                                          >
                                            ยังไม่ส่ง
                                          </MenuItem>
                                        </Select>
                                      </FormControl>
                                    </div>
                                    <div className="col-xs-6 col-sm-6 col-md-4 mb-3 ml-auto">
                                      <FormControl
                                        fullWidth
                                        className={classes.selectFormControl}
                                      >
                                        <InputLabel
                                          htmlFor="selectsale"
                                          className={classes.selectLabel}
                                        >
                                          เลือกฝ่ายขาย
                                        </InputLabel>
                                        <Select
                                          MenuProps={{
                                            className: classes.selectMenu
                                          }}
                                          classes={{
                                            select: classes.select
                                          }}
                                          value={this.state.selectsale}
                                          onChange={this.handleSelectSale_1_3}
                                          inputProps={{
                                            name: "selectsale",
                                            id: "selectsale"
                                          }}
                                        >
                                          <MenuItem key={-1} value={"all"}>
                                            {"ทั้งหมด"}
                                          </MenuItem>
                                          {this.state.sales}
                                        </Select>
                                      </FormControl>
                                    </div>
                                  </div>
                                  <div className="mt-4">
                                    <MaterialTable
                                      onRowClick={(e, d) =>
                                        this.showContactModal(d)
                                      }
                                      columns={[
                                        {
                                          title: "สถานะ",
                                          field: "status",
                                          defaultSort: "desc"
                                        },
                                        {
                                          title: "ชื่อบริษัท",
                                          field: "company_thname"
                                        },
                                        {
                                          title: "ชื่อลูกค้า",
                                          field: "contact_name"
                                        },
                                        {
                                          title: "เบอร์ติดต่อ",
                                          field: "contact_no"
                                        },
                                        {
                                          title: "อีเมล์",
                                          field: "contact_email"
                                        },
                                        {
                                          title: "ชื่อ Call Center",
                                          field: "call_center_name"
                                        },
                                        {
                                          title: "ชื่อฝ่ายขาย",
                                          field: "sale_name"
                                        }
                                      ]}
                                      data={this.state.data1_3}
                                      title="สถานะ 1.3 - ส่งข้อมูล"
                                    />
                                  </div>
                                </div>
                              )
                            },
                            {
                              tabName: "สถานะ 1.4",
                              tabContent: (
                                <div className="mt-4">
                                  <h4>
                                    จำนวนลูกค้าทั้งหมด{" "}
                                    {this.state.data1_4.length} ราย
                                  </h4>
                                  <div className="row ml-auto mr-auto justify-content-end">
                                    <div className="col-xs-12 col-sm-6 col-md-4 mb-3">
                                      <FormControl
                                        fullWidth
                                        className={classes.selectFormControl}
                                      >
                                        <InputLabel
                                          htmlFor="selectsale"
                                          className={classes.selectLabel}
                                        >
                                          เลือกฝ่ายขาย
                                        </InputLabel>
                                        <Select
                                          MenuProps={{
                                            className: classes.selectMenu
                                          }}
                                          classes={{
                                            select: classes.select
                                          }}
                                          value={this.state.selectsale}
                                          onChange={this.handleSelectSale}
                                          inputProps={{
                                            name: "selectsale",
                                            id: "selectsale"
                                          }}
                                        >
                                          <MenuItem key={-1} value={"all"}>
                                            {"ทั้งหมด"}
                                          </MenuItem>
                                          {this.state.sales}
                                        </Select>
                                      </FormControl>
                                    </div>
                                  </div>
                                  <div className="mt-4">
                                    <MaterialTable
                                      onRowClick={(e, d) =>
                                        this.showContactModal(d)
                                      }
                                      columns={[
                                        {
                                          title: "สถานะ",
                                          field: "status",
                                          defaultSort: "desc"
                                        },
                                        {
                                          title: "ชื่อบริษัท",
                                          field: "company_thname"
                                        },
                                        {
                                          title: "ชื่อลูกค้า",
                                          field: "contact_name"
                                        },
                                        {
                                          title: "เบอร์ติดต่อ",
                                          field: "contact_no"
                                        },
                                        {
                                          title: "อีเมล์",
                                          field: "contact_email"
                                        },
                                        {
                                          title: "ชื่อ Call Center",
                                          field: "call_center_name"
                                        },
                                        {
                                          title: "ชื่อฝ่ายขาย",
                                          field: "sale_name"
                                        }
                                      ]}
                                      data={this.state.data1_4}
                                      title="สถานะ 1.4 - ลูกค้าให้เข้าพบ"
                                    />
                                  </div>
                                </div>
                              )
                            }
                          ]}
                        />
                      </GridItem>
                    </GridContainer>
                  )
                },
                {
                  tabName: "สถานะ 2",
                  tabIcon: TransferWithinAStation,
                  tabContent: (
                    <div className="mt-4">
                      <h4>จำนวนลูกค้าทั้งหมด {this.state.data2.length} ราย</h4>
                      <div className="row ml-auto mr-auto justify-content-end">
                        <div className="col-xs-12 col-sm-6 col-md-4 mb-3">
                          <FormControl
                            fullWidth
                            className={classes.selectFormControl}
                          >
                            <InputLabel
                              htmlFor="selectsale"
                              className={classes.selectLabel}
                            >
                              เลือกฝ่ายขาย
                            </InputLabel>
                            <Select
                              MenuProps={{
                                className: classes.selectMenu
                              }}
                              classes={{
                                select: classes.select
                              }}
                              value={this.state.selectsale}
                              onChange={this.handleSelectSale}
                              inputProps={{
                                name: "selectsale",
                                id: "selectsale"
                              }}
                            >
                              <MenuItem key={-1} value={"all"}>
                                {"ทั้งหมด"}
                              </MenuItem>
                              {this.state.sales}
                            </Select>
                          </FormControl>
                        </div>
                      </div>
                      <div className="mt-4">
                        <MaterialTable
                          onRowClick={(e, d) => this.showContactModal(d)}
                          columns={[
                            {
                              title: "สถานะ",
                              field: "status",
                              defaultSort: "desc"
                            },
                            { title: "ชื่อบริษัท", field: "company_thname" },
                            { title: "ชื่อลูกค้า", field: "contact_name" },
                            { title: "เบอร์ติดต่อ", field: "contact_no" },
                            { title: "อีเมล์", field: "contact_email" },
                            {
                              title: "ชื่อ Call Center",
                              field: "call_center_name"
                            },
                            { title: "ชื่อฝ่ายขาย", field: "sale_name" }
                          ]}
                          data={this.state.data2}
                          title="สถานะ 2 - เข้าพบลูกค้าแล้ว"
                        />
                      </div>
                    </div>
                  )
                },
                {
                  tabName: "สถานะ 3",
                  tabIcon: MonetizationOn,
                  tabContent: (
                    <div className="mt-4">
                      <h4>จำนวนลูกค้าทั้งหมด {this.state.data3.length} ราย</h4>
                      <div className="row ml-auto mr-auto justify-content-end">
                        <div className="col-xs-12 col-sm-6 col-md-4 mb-3">
                          <FormControl
                            fullWidth
                            className={classes.selectFormControl}
                          >
                            <InputLabel
                              htmlFor="selectsale"
                              className={classes.selectLabel}
                            >
                              เลือกฝ่ายขาย
                            </InputLabel>
                            <Select
                              MenuProps={{
                                className: classes.selectMenu
                              }}
                              classes={{
                                select: classes.select
                              }}
                              value={this.state.selectsale}
                              onChange={this.handleSelectSale}
                              inputProps={{
                                name: "selectsale",
                                id: "selectsale"
                              }}
                            >
                              <MenuItem key={-1} value={"all"}>
                                {"ทั้งหมด"}
                              </MenuItem>
                              {this.state.sales}
                            </Select>
                          </FormControl>
                        </div>
                      </div>
                      <div className="mt-4">
                        <MaterialTable
                          onRowClick={(e, d) => this.showContactModal(d)}
                          columns={[
                            {
                              title: "สถานะ",
                              field: "status",
                              defaultSort: "desc"
                            },
                            { title: "ชื่อบริษัท", field: "company_thname" },
                            { title: "ชื่อลูกค้า", field: "contact_name" },
                            { title: "เบอร์ติดต่อ", field: "contact_no" },
                            { title: "อีเมล์", field: "contact_email" },
                            {
                              title: "ชื่อ Call Center",
                              field: "call_center_name"
                            },
                            { title: "ชื่อฝ่ายขาย", field: "sale_name" }
                          ]}
                          data={this.state.data3}
                          title="สถานะ 3 - เสนอราคา"
                        />
                      </div>
                    </div>
                  )
                },
                {
                  tabName: "สถานะ 4",
                  tabIcon: ShoppingCart,
                  tabContent: (
                    <div className="mt-4">
                      <h4>จำนวนลูกค้าทั้งหมด {this.state.data4.length} ราย</h4>
                      <div className="row ml-auto mr-auto justify-content-end">
                        <div className="col-xs-12 col-sm-6 col-md-4 mb-3">
                          <FormControl
                            fullWidth
                            className={classes.selectFormControl}
                          >
                            <InputLabel
                              htmlFor="selectsale"
                              className={classes.selectLabel}
                            >
                              เลือกฝ่ายขาย
                            </InputLabel>
                            <Select
                              MenuProps={{
                                className: classes.selectMenu
                              }}
                              classes={{
                                select: classes.select
                              }}
                              value={this.state.selectsale}
                              onChange={this.handleSelectSale}
                              inputProps={{
                                name: "selectsale",
                                id: "selectsale"
                              }}
                            >
                              <MenuItem key={-1} value={"all"}>
                                {"ทั้งหมด"}
                              </MenuItem>
                              {this.state.sales}
                            </Select>
                          </FormControl>
                        </div>
                      </div>
                      <div className="mt-4">
                        <MaterialTable
                          onRowClick={(e, d) => this.showContactModal(d)}
                          columns={[
                            {
                              title: "สถานะ",
                              field: "status",
                              defaultSort: "desc"
                            },
                            { title: "ชื่อบริษัท", field: "company_thname" },
                            { title: "ชื่อลูกค้า", field: "contact_name" },
                            { title: "เบอร์ติดต่อ", field: "contact_no" },
                            { title: "อีเมล์", field: "contact_email" },
                            {
                              title: "ชื่อ Call Center",
                              field: "call_center_name"
                            },
                            { title: "ชื่อฝ่ายขาย", field: "sale_name" }
                          ]}
                          data={this.state.data4}
                          title="สถานะ 4 - เกิดการสั่งซื้อ"
                        />
                      </div>
                    </div>
                  )
                },
                {
                  tabName: "สถานะ 5",
                  tabIcon: Cached,
                  tabContent: (
                    <div className="mt-4">
                      <h4>จำนวนลูกค้าทั้งหมด {this.state.data5.length} ราย</h4>
                      <div className="row ml-auto mr-auto justify-content-end">
                        <div className="col-xs-12 col-sm-6 col-md-4 mb-3">
                          <FormControl
                            fullWidth
                            className={classes.selectFormControl}
                          >
                            <InputLabel
                              htmlFor="selectsale"
                              className={classes.selectLabel}
                            >
                              เลือกฝ่ายขาย
                            </InputLabel>
                            <Select
                              MenuProps={{
                                className: classes.selectMenu
                              }}
                              classes={{
                                select: classes.select
                              }}
                              value={this.state.selectsale}
                              onChange={this.handleSelectSale}
                              inputProps={{
                                name: "selectsale",
                                id: "selectsale"
                              }}
                            >
                              <MenuItem key={-1} value={"all"}>
                                {"ทั้งหมด"}
                              </MenuItem>
                              {this.state.sales}
                            </Select>
                          </FormControl>
                        </div>
                      </div>
                      <div className="mt-4">
                        <MaterialTable
                          onRowClick={(e, d) => this.showContactModal(d)}
                          columns={[
                            {
                              title: "สถานะ",
                              field: "status",
                              defaultSort: "desc"
                            },
                            { title: "ชื่อบริษัท", field: "company_thname" },
                            { title: "ชื่อลูกค้า", field: "contact_name" },
                            { title: "เบอร์ติดต่อ", field: "contact_no" },
                            { title: "อีเมล์", field: "contact_email" },
                            {
                              title: "ชื่อ Call Center",
                              field: "call_center_name"
                            },
                            { title: "ชื่อฝ่ายขาย", field: "sale_name" }
                          ]}
                          data={this.state.data5}
                          title="สถานะ 5 - ซื้อซ้ำ"
                        />
                      </div>
                    </div>
                  )
                }
              ]}
            />
          </GridItem>
        </GridContainer>

        <Modal isOpen={this.state.modal} toggledatatable={this.toggledatatable}>
          <ModalHeader toggle={this.toggledatatable}>ข้อมูลลูกค้า</ModalHeader>
          <ModalBody>
            <div className="row">
              <div className="col-xs-12 col-sm-6 col-md-6">
                <Form>
                  <FormGroup>
                    <h6 style={{ color: "#8A8A8A" }}>
                      สถานะการส่งอีเมล์:{" "}
                      <Label style={{ fontSize: "20px" }}>ยังไม่ส่ง</Label>{" "}
                    </h6>{" "}
                    <Button color="success" onClick={this.toggleReceiveEmail}>
                      <Icon className="pt-1">email</Icon>
                      ส่งอีเมล์
                    </Button>
                  </FormGroup>
                </Form>
              </div>
              <div className="col-xs-12 col-sm-6 col-md-6 text-right pr-4">
                <Button color="danger" onClick={this.toggleDeleteCompany}>
                  <Icon className="pt-1">delete</Icon>
                  ลบข้อมูลบริษัท
                </Button>
              </div>
            </div>
            <GridContainer>
              <GridItem xs={12} sm={12} md={12}>
                <CustomTabs
                  className="table-responsive"
                  headerColor="info"
                  tabs={[
                    {
                      // สถานะ 0
                      tabName: "ข้อมูลบริษัท",
                      tabIcon: Home,
                      tabContent: (
                        <div className="mt-4">
                          <Button color="warning" style={{ color: "white" }}>
                            <Icon className="pt-1">edit</Icon> แก้ไข{" "}
                          </Button>{" "}
                          <Button
                            color="danger"
                            style={{ color: "white", display: "none" }}
                          >
                            <Icon className="pt-1">close</Icon> ยกเลิก{" "}
                          </Button>{" "}
                          <GridContainer>
                            <GridItem xs={12} sm={12} md={6}>
                              <CustomInput
                                labelText="Tax ID: "
                                id="taxid"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact.tax_id,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={6}>
                              <CustomInput
                                labelText="ID บริษัท: "
                                placeholder="asdasd"
                                id="companyid"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact
                                    .company_id,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={6}>
                              <CustomInput
                                labelText="ชื่อภาษาไทย: "
                                id="companynameth"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact
                                    .company_thname,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={6}>
                              <CustomInput
                                labelText="ชื่อภาษาอังกฤษ: "
                                id="companynameen"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact
                                    .company_enname,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                          </GridContainer>
                          <GridContainer>
                            <GridItem xs={12} sm={12} md={4}>
                              <CustomInput
                                labelText="กลุ่มของบริษัท (ชื่อภาษาไทย): "
                                id="groupth"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact
                                    .group_thname,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={4}>
                              <CustomInput
                                labelText="กลุ่มของบริษัท (ชื่อภาษาอังกฤษ): "
                                id="groupen"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact
                                    .group_enname,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={4}>
                              <CustomInput
                                labelText="Parent: "
                                id="parent"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact
                                    .parent_account,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                          </GridContainer>
                          <GridContainer>
                            <GridItem xs={12} sm={12} md={3}>
                              <CustomInput
                                labelText="Address NO: "
                                id="address"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact
                                    .address_no,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={3}>
                              <CustomInput
                                labelText="หมู่: "
                                id="villageno"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact.moo,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={3}>
                              <CustomInput
                                labelText="ซอย: "
                                id="soy"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact.soi,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={3}>
                              <CustomInput
                                labelText="หมู่บ้าน: "
                                id="village"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact.village,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                          </GridContainer>
                          <GridContainer>
                            <GridItem xs={12} sm={12} md={3}>
                              <CustomInput
                                labelText="ถนน: "
                                id="street"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact.road,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={3}>
                              <CustomInput
                                labelText="แขวง/ตำบล: "
                                id="subdistrict"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact
                                    .subdistrict,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={3}>
                              <CustomInput
                                labelText="เขต/เมือง: "
                                id="district"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact.district,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={3}>
                              <CustomInput
                                labelText="จังหวัด: "
                                id="province"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact.province,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                          </GridContainer>
                          <GridContainer>
                            <GridItem xs={12} sm={12} md={4}>
                              <CustomInput
                                labelText="ไปรษณีย์: "
                                id="Postalcode"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact.zipcode,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                          </GridContainer>
                        </div>
                      )
                    },
                    {
                      tabName: "ข้อมูลผู้ติดต่อ",
                      tabIcon: AccountBox,
                      tabContent: (
                        <div className="mt-4">
                          <Button color="warning" style={{ color: "white" }}>
                            <Icon className="pt-1">edit</Icon> แก้ไข{" "}
                          </Button>{" "}
                          <Button
                            color="danger"
                            style={{ color: "white", display: "none" }}
                          >
                            <Icon className="pt-1">close</Icon> ยกเลิก{" "}
                          </Button>{" "}
                          <GridContainer>
                            <GridItem xs={12} sm={12} md={6}>
                              <CustomInput
                                labelText="Contact Name: "
                                id="contactname"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact
                                    .contact_name,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={6}>
                              <CustomInput
                                labelText="Phone No.: "
                                id="phone"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact
                                    .contact_no,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={6}>
                              <CustomInput
                                labelText="Email: "
                                id="email"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact
                                    .contact_email,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={6}>
                              <CustomInput
                                labelText="Type: "
                                id="type"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact.type,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                          </GridContainer>
                          <GridContainer>
                            <GridItem xs={12} sm={12} md={8}>
                              <CustomInput
                                labelText="Customer Product: "
                                id="customer"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact
                                    .customer_product,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                          </GridContainer>
                          <GridContainer>
                            <GridItem xs={12} sm={12} md={6}>
                              <CustomInput
                                labelText="Contact Name 2: "
                                id="contactname2"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact
                                    .contact_name_2,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={6}>
                              <CustomInput
                                labelText="Phone No. 2: "
                                id="phone2"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact
                                    .contact_no_2,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={6}>
                              <CustomInput
                                labelText="Email 2: "
                                id="email2"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact
                                    .contact_email_2,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={6}>
                              <CustomInput
                                labelText="Grade: "
                                id="grade"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact.grade,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                          </GridContainer>
                          <GridContainer>
                            <GridItem xs={12} sm={12} md={8}>
                              <CustomInput
                                labelText="Customer Product 2: "
                                id="customer2"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact
                                    .customer_product_2,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                          </GridContainer>
                        </div>
                      )
                    },
                    {
                      tabName: "ข้อมูลฝ่ายขาย",
                      tabIcon: Person,
                      tabContent: (
                        <div className="mt-4">
                          <Button color="warning" style={{ color: "white" }}>
                            <Icon className="pt-1">edit</Icon> แก้ไข{" "}
                          </Button>{" "}
                          <Button
                            color="danger"
                            style={{ color: "white", display: "none" }}
                          >
                            <Icon className="pt-1">close</Icon> ยกเลิก{" "}
                          </Button>{" "}
                          <GridContainer>
                            <GridItem xs={12} sm={12} md={6}>
                              <CustomInput
                                labelText="Sale Name: "
                                id="salename"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact
                                    .sale_name,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={6}>
                              <CustomInput
                                labelText="Phone No. (Sale): "
                                id="phonesale"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact
                                    .sale_phone,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={6}>
                              <CustomInput
                                labelText="Email (Sale): "
                                id="emailsale"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact
                                    .sale_name,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={6}>
                              <CustomInput
                                labelText="Call Center Name: "
                                id="callcentername"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact
                                    .call_center_name,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                          </GridContainer>
                        </div>
                      )
                    },
                    {
                      tabName: "ข้อมูลทางบัญชี",
                      tabIcon: Assessment,
                      tabContent: (
                        <div className="mt-4">
                          <Button color="warning" style={{ color: "white" }}>
                            <Icon className="pt-1">edit</Icon> แก้ไข{" "}
                          </Button>{" "}
                          <Button
                            color="danger"
                            style={{ color: "white", display: "none" }}
                          >
                            <Icon className="pt-1">close</Icon> ยกเลิก{" "}
                          </Button>{" "}
                          <GridContainer>
                            <GridItem xs={12} sm={12} md={4}>
                              <CustomInput
                                labelText="Financial Grade: "
                                id="finangrade"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact
                                    .financial_grade,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={4}>
                              <CustomInput
                                labelText="Billing Name: "
                                id="billname"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact
                                    .billing_name,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={4}>
                              <CustomInput
                                labelText="Billing Place: "
                                id="billplace"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  // onChange: this.handleChange,
                                  // name: "username",
                                  // type: "text",
                                  defaultValue: this.state.tmp_contact
                                    .billing_place,
                                  disabled: true
                                }}
                              />
                            </GridItem>
                          </GridContainer>
                          <GridContainer>
                            <GridItem xs={12} sm={12} md={4}>
                              <CustomInput
                                labelText="Billing Create Date: "
                                id="createdate"
                                formControlProps={{
                                  fullWidth: true
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={4}>
                              <CustomInput
                                labelText="Cheque Receive Date: "
                                id="receivedate"
                                formControlProps={{
                                  fullWidth: true
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={4}>
                              <CustomInput
                                labelText="Finance Officer Name: "
                                id="officename"
                                formControlProps={{
                                  fullWidth: true
                                }}
                              />
                            </GridItem>
                          </GridContainer>
                          <GridContainer>
                            <GridItem xs={12} sm={12} md={4}>
                              <CustomInput
                                labelText="Phone No.: "
                                id="phonenoaccout"
                                formControlProps={{
                                  fullWidth: true
                                }}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={4}>
                              <CustomInput
                                labelText="Email: "
                                id="emailaccount"
                                formControlProps={{
                                  fullWidth: true
                                }}
                              />
                            </GridItem>
                          </GridContainer>
                        </div>
                      )
                    },
                    {
                      tabName: "โน๊ต",
                      tabIcon: EventNote,
                      tabContent: (
                        <div className="mt-4">
                          <GridContainer>
                            <GridItem xs={12} sm={12} md={12}>
                              <CustomInput
                                labelText="เพิ่มโน๊ตที่นี่..."
                                id="addnote"
                                formControlProps={{
                                  fullWidth: true
                                }}
                                inputProps={{
                                  multiline: true,
                                  rows: 3
                                }}
                              />
                            </GridItem>
                          </GridContainer>
                          <GridContainer>
                            <GridItem xs={6} sm={3} md={2}>
                              <Button color="info" style={{ color: "white" }}>
                                <Icon className="pt-1">note_add</Icon> เพิ่มโน๊ต{" "}
                              </Button>{" "}
                            </GridItem>
                            <GridItem xs={6} sm={3} md={3}>
                              <Input
                                type="file"
                                name="file"
                                className="mt-2"
                                id="exampleFile"
                              />
                            </GridItem>
                            <GridItem xs={6} sm={3} md={2}>
                              <Button
                                color="warning"
                                style={{ color: "white" }}
                                onClick={this.toggleNotify}
                              >
                                <Icon className="pt-1">
                                  notifications_active
                                </Icon>{" "}
                                แจ้งเตือน{" "}
                              </Button>{" "}
                            </GridItem>
                          </GridContainer>
                          <GridContainer>
                            <GridItem xs={12} sm={12} md={12}>
                              <div className="mt-3">
                                <MaterialTable
                                  title="ประวัติการเปลี่ยนแปลง"
                                  columns={[
                                    {
                                      title: "สถานะ",
                                      field: "status",
                                      defaultSort: "desc"
                                    },
                                    {
                                      title: "กรอกโดย",
                                      field: "create_by"
                                    },
                                    {
                                      title: "รายละเอียด",
                                      render: rowdata => {
                                        return (
                                          <div
                                            dangerouslySetInnerHTML={{
                                              __html: rowdata.text
                                            }}
                                          />
                                        );
                                      }
                                    },
                                    {
                                      title: "เอกสาร",
                                      render: rowdata => {
                                        return (
                                          <div
                                            dangerouslySetInnerHTML={{
                                              __html: rowdata.doc
                                            }}
                                          />
                                        );
                                      }
                                    },
                                    { title: "วันที่", field: "create_date" }
                                  ]}
                                  data={this.state.notes}
                                />
                              </div>
                            </GridItem>
                          </GridContainer>
                        </div>
                      )
                    }
                  ]}
                />
              </GridItem>
            </GridContainer>
          </ModalBody>
          <ModalFooter>
            <FormControl
              style={{ width: "200px" }}
              className={classes.selectFormControl}
            >
              <InputLabel
                htmlFor="simple-select"
                className={classes.selectLabel}
              >
                เปลี่ยนสถานะ
              </InputLabel>
              <Select
                MenuProps={{
                  className: classes.selectMenu
                }}
                classes={{
                  select: classes.select
                }}
                value={this.state.status}
                onChange={this.handleChangeStatus}
                onOpen={this.handleChangeStatus}
                inputProps={{
                  name: "status",
                  id: "status"
                }}
              >
                <MenuItem
                  classes={{
                    root: classes.selectMenuItem,
                    selected: classes.selectMenuItemSelected
                  }}
                  value="0"
                  onClick={this.toggleChangeStatus}
                >
                  0 - ยังไม่มีการติดต่อ
                </MenuItem>
                <MenuItem
                  classes={{
                    root: classes.selectMenuItem,
                    selected: classes.selectMenuItemSelected
                  }}
                  value="1.1"
                  onClick={this.toggleChangeStatus}
                >
                  1.1 - ยังไม่สนใจ
                </MenuItem>
                <MenuItem
                  classes={{
                    root: classes.selectMenuItem,
                    selected: classes.selectMenuItemSelected
                  }}
                  value="1.2"
                  onClick={this.toggleChangeStatus}
                >
                  1.2 - Call Center ติดตาม
                </MenuItem>
                <MenuItem
                  classes={{
                    root: classes.selectMenuItem,
                    selected: classes.selectMenuItemSelected
                  }}
                  value="1.3"
                  onClick={this.toggleChangeStatus}
                >
                  1.3 - ส่งข้อมูล
                </MenuItem>
                <MenuItem
                  classes={{
                    root: classes.selectMenuItem,
                    selected: classes.selectMenuItemSelected
                  }}
                  value="1.4"
                  onClick={this.toggleChangeStatus}
                >
                  1.4 - ลูกค้าให้เข้าพบ
                </MenuItem>
                <MenuItem
                  classes={{
                    root: classes.selectMenuItem,
                    selected: classes.selectMenuItemSelected
                  }}
                  value="2"
                  onClick={this.toggleChangeStatus}
                >
                  2 - เข้าพบลูกค้าแล้ว
                </MenuItem>
                <MenuItem
                  classes={{
                    root: classes.selectMenuItem,
                    selected: classes.selectMenuItemSelected
                  }}
                  value="3"
                  onClick={this.toggleChangeStatus}
                >
                  3 - เสนอราคา
                </MenuItem>
                <MenuItem
                  classes={{
                    root: classes.selectMenuItem,
                    selected: classes.selectMenuItemSelected
                  }}
                  value="4"
                  onClick={this.toggleChangeStatus}
                >
                  4 - ลูกค้าซื้อ
                </MenuItem>
                <MenuItem
                  classes={{
                    root: classes.selectMenuItem,
                    selected: classes.selectMenuItemSelected
                  }}
                  value="5"
                  onClick={this.toggleChangeStatus}
                >
                  5 - ข้อมูลซื้อซ้ำ
                </MenuItem>
              </Select>
            </FormControl>
            <Button
              color="secondary"
              style={{ margin: "15px" }}
              onClick={this.toggledatatable}
            >
              ปิด
            </Button>
          </ModalFooter>
        </Modal>

        {/* Modal Upload */}
        <Modal
          className="modal-upload"
          isOpen={this.state.modalUpload}
          toggleUpload={this.toggleUpload}
        >
          <ModalHeader toggle={this.toggleUpload}>
            อัพโหลดไฟล์ Excel
          </ModalHeader>
          <ModalBody>
            <FormGroup>
              <Label for="exampleFile">File</Label> <br />
              <Label for="exampleFile">เลือกไฟล์ (.xlsx, .xls เท่านั้น)</Label>
              <Input
                type="file"
                name="file"
                id="exampleFile"
                style={{ border: "2px solid #f5f5f5" }}
              />
              <FormText color="muted">
                ดาวน์โหลดไฟล์เทมเพลต :
                <span style={{ color: "green" }}>Excel</span> <br />
                <span style={{ color: "red" }}>
                  *หมายเหตุ: ถ้าไม่ระบุ tax id ระบบจะไม่นำเข้าข้อมูล
                </span>
              </FormText>
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.toggleUpload}>
              อัพโหลด
            </Button>{" "}
            <Button color="secondary" onClick={this.toggleUpload}>
              ปิด
            </Button>
          </ModalFooter>
        </Modal>

        {/*Modal Export */}
        <Modal
          className="modal-export"
          isOpen={this.state.modalExport}
          toggleExport={this.toggleExport}
        >
          <ModalHeader toggle={this.toggleExport}>
            เลือกรูปแบบรายงาน
          </ModalHeader>
          <ModalBody>
            <Form>
              <FormGroup>
                <Label for="exampleDate">เริ่ม</Label>
                <Input
                  type="date"
                  name="dateStart"
                  id="exampleDate"
                  placeholder="date placeholder"
                />
              </FormGroup>
              <FormGroup>
                <Label for="exampleDate">ถึง</Label>
                <Input
                  type="date"
                  name="dateTo"
                  id="exampleDate"
                  placeholder="date placeholder"
                />
              </FormGroup>
              <FormGroup check>
                <Label check className="font-check">
                  <Input type="checkbox" className="checkbox" /> เลือกทั้งหมด
                </Label>
              </FormGroup>
              <FormGroup check>
                <Label check className="font-check">
                  <Input type="checkbox" className="checkbox" /> สถานะ 0 -
                  ยังไม่มีการติดต่อ
                </Label>
              </FormGroup>
              <FormGroup check>
                <Label check className="font-check">
                  <Input type="checkbox" className="checkbox" /> สถานะ 1 -
                  โทรติดต่อลูกค้าแล้ว
                </Label>
              </FormGroup>
              <FormGroup check>
                <Label check className="font-check">
                  <Input type="checkbox" className="checkbox" /> สถานะ 1.1 -
                  ยังไม่สนใจ
                </Label>
              </FormGroup>
              <FormGroup check>
                <Label check className="font-check">
                  <Input type="checkbox" className="checkbox" /> สถานะ 1.2 -
                  Call Center ติดตาม
                </Label>
              </FormGroup>
              <FormGroup check>
                <Label check className="font-check">
                  <Input type="checkbox" className="checkbox" /> สถานะ 1.3 -
                  ส่งข้อมูล
                </Label>
              </FormGroup>
              <FormGroup check>
                <Label check className="font-check">
                  <Input type="checkbox" className="checkbox" /> สถานะ 1.4 -
                  ลูกค้าให้เข้าพบ
                </Label>
              </FormGroup>
              <FormGroup check>
                <Label check className="font-check">
                  <Input type="checkbox" className="checkbox" /> สถานะ 2 -
                  เข้าพบลูกค้าแล้ว
                </Label>
              </FormGroup>
              <FormGroup check>
                <Label check className="font-check">
                  <Input type="checkbox" className="checkbox" /> สถานะ 3 -
                  เสนอราคา
                </Label>
              </FormGroup>
              <FormGroup check>
                <Label check className="font-check">
                  <Input type="checkbox" className="checkbox" /> สถานะ 4 -
                  เกิดการสั่งซื้อ
                </Label>
              </FormGroup>
              <FormGroup check>
                <Label check className="font-check">
                  <Input type="checkbox" className="checkbox" /> สถานะ 5 -
                  ซื้อซ้ำ
                </Label>
              </FormGroup>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.toggleExport}>
              ออกรายงาน
            </Button>{" "}
            <Button color="secondary" onClick={this.toggleExport}>
              ปิด
            </Button>
          </ModalFooter>
        </Modal>

        {/*Modal delete info company */}
        <Modal
          className="modal-export"
          isOpen={this.state.modalDeleteCompany}
          toggle={this.toggleDeleteCompany}
        >
          <ModalHeader toggle={this.toggleDeleteCompany}>
            <Warning style={{ color: "red" }} />
            ลบข้อมูลบริษัท !
          </ModalHeader>
          <ModalBody>
            <h6>คุณต้องการลบข้อมูลบริษัทหรือไม่</h6>
          </ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={this.toggleDeleteCompany}>
              ยืนยัน
            </Button>{" "}
            <Button color="secondary" onClick={this.toggleDeleteCompany}>
              ยกเลิก
            </Button>
          </ModalFooter>
        </Modal>

        {/*Modal Change Status */}
        <Modal
          className="modal-export"
          isOpen={this.state.modalChangeStatus}
          toggle={this.toggleChangeStatus}
        >
          <ModalHeader toggle={this.toggleChangeStatus}>
            <Warning style={{ color: "#FFC107" }} />
            เปลี่ยนสถานะ
          </ModalHeader>
          <ModalBody>
            <h6>คุณได้ทำการเปลี่ยนสถานะ</h6>
          </ModalBody>
          <ModalFooter>
            <Button color="success" onClick={this.toggleChangeStatus}>
              ยืนยัน
            </Button>{" "}
            {/* <Button color="secondary" onClick={this.toggleChangeStatus}>
              ยกเลิก
            </Button> */}
          </ModalFooter>
        </Modal>

        {/*Modal Notify */}
        <Modal
          className="modal-export"
          isOpen={this.state.modalNotify}
          toggle={this.toggleNotify}
        >
          <ModalHeader toggle={this.toggleNotify}>
            <Warning style={{ color: "#FFC107" }} />
            แจ้งเตือน
          </ModalHeader>
          <ModalBody>
            <Form>
              <FormGroup>
                <Label for="textNotify">ข้อความแจ้งเตือน</Label>
                <Input type="textarea" name="textnotify" id="textNotify" />
              </FormGroup>
              <FormGroup>
                <Label for="notifydate">วันที่แจ้งเตือน</Label>
                <Input
                  type="date"
                  name="date"
                  id="notifydate"
                  placeholder="กรุณาเลือกวันที่"
                />
              </FormGroup>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button color="success" onClick={this.toggleNotify}>
              ยืนยัน
            </Button>{" "}
            {/* <Button color="secondary" onClick={this.toggleChangeStatus}>
              ยกเลิก
            </Button> */}
          </ModalFooter>
        </Modal>

        {/*Modal Receive Email */}
        <Modal
          className="modal-export"
          isOpen={this.state.modalReceiveEmail}
          toggle={this.toggleReceiveEmail}
        >
          <ModalHeader toggle={this.toggleReceiveEmail}>
            <Warning style={{ color: "#FFC107" }} />
            ยืนยัน
          </ModalHeader>
          <ModalBody>
            <h6>ยืนยันการส่งอีเมล์</h6>
          </ModalBody>
          <ModalFooter>
            <Button color="success" onClick={this.toggleReceiveEmail}>
              ยืนยัน
            </Button>{" "}
            <Button color="secondary" onClick={this.toggleReceiveEmail}>
              ยกเลิก
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

TableList.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(TableList);
