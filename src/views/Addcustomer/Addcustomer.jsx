/* eslint-disable */
import React from "react";
// @material-ui/core components
// import withStyles from "@material-ui/core/styles/withStyles";
// @material-ui/icons
// import AddAlert from "@material-ui/icons/AddAlert";
// core components
// import GridItem from "components/Grid/GridItem.jsx";
// import GridContainer from "components/Grid/GridContainer.jsx";
import Icon from "@material-ui/core/Icon";
// import Button from "components/CustomButtons/Button.jsx";
// import SnackbarContent from "components/Snackbar/SnackbarContent.jsx";
// import Snackbar from "components/Snackbar/Snackbar.jsx";
import Card from "components/Card/Card.jsx";
// import CardHeader from "components/Card/CardHeader.jsx";
// import CardBody from "components/Card/CardBody.jsx";
import { Row, Col, Button, Form, FormGroup, Input, Label } from "reactstrap";
import "../../assets/css/addcustomer.css";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

import axios from 'axios';
import conf from '../../conf.json'

const styles = theme => ({
  button: {
    display: "block",
    marginTop: theme.spacing.unit * 2
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  }
});

export default class AddCustomer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      salename: "",
      callcentername: "",
      callcenter: [],
      sales: [],
      selectsale: "none",
      selectCallcenter: "none",
      salesData: [],
      tel: "",
      emailSale: "",
      taxid: "",
    };
    this.getSales = this.getSales.bind(this);
    this.getSales();

    this.getCallCenters = this.getCallCenters.bind(this);
    this.getCallCenters();

    this.handleInput = this.handleInput.bind(this);
  }
  getSales() {
    axios
      .get(conf.url + "/sale", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        console.log(res)
        let sales = res.data.data.map((data, index) => {
          return (
            <MenuItem tel={data.actelephone} key={index} value={data.acusername}>
              {data.acfirstname + " " + data.aclastname}
            </MenuItem>
          );
        });
        this.setState({
          sales: sales,
          salesData: res.data.data
        });
      });
  }
  getCallCenters() {
    axios
      .get(conf.url + "/callcenter", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        console.log(res)
        let callcenter = res.data.data.map((data, index) => {
          return (
            <MenuItem tel={data.actelephone} key={index} value={data.acusername}>
              {data.acfirstname + " " + data.aclastname}
            </MenuItem>
          );
        });
        this.setState({
          callcenter: callcenter,
        });
      });
  }
  handleInput = event => {
    this.setState({ [event.target.name]: event.target.value });
  };
  handleSalename = event => {
    this.setState({ [event.target.name]: event.target.value });
    if( event.target.value !== "none"){
      this.state.salesData.map(data=>{
        if(data.acusername === event.target.value)
          this.setState({
            tel: data.actelephone,
            emailSale: data.acemail
          })
      })
    }else{
      this.setState({
        tel: "",
        emailSale: ""
      })
    }
    
    
  };

  handleCallcentername = event => {
    this.setState({ [event.target.name]: event.target.value });
  };
  render() {
    const { classes } = this.props;
    return (
      <div>
        <Card className="card-box">
          <Row>
            <Col xs="12" sm="12" md="12" lg="12" className="text-left">
              <Form>
                <h6>1. ข้อมูลบริษัท</h6>
                <Row>
                  <Col xs="12" sm="6" md="4" lg="4">
                    <FormGroup>
                      <Label for="taxID">
                        Tax ID (หากไม่มีเลข Tax ID ให้ระบุเหตุผล)
                      </Label>
                      <Input value={this.state.taxid} onChange={this.handleInput} type="text" name="taxid" id="taxid" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="4" lg="4">
                    <FormGroup>
                      <Label for="companyID">ID บริษัท</Label>
                      <Input type="text" name="companyID" id="companyID" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="4" lg="4">
                    <FormGroup>
                      <Label for="status">สถานะ</Label>
                      <Input type="text" name="status" id="status" />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label for="thaiName">ชื่อภาษาไทย</Label>
                      <Input type="text" name="thaiName" id="thaiName" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label for="engName">ชื่อภาษาอังกฤษ</Label>
                      <Input type="text" name="engName" id="engName" />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="6" md="4" lg="4">
                    <FormGroup>
                      <Label for="groupNameth">
                        กลุ่มของบริษัท (ชื่อภาษาไทย)
                      </Label>
                      <Input type="text" name="groupNameth" id="groupNameth" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="4" lg="4">
                    <FormGroup>
                      <Label for="groupNameen">
                        กลุ่มของบริษัท (ชื่อภาษาอังกฤษ)
                      </Label>
                      <Input type="text" name="groupNameen" id="groupNameen" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="4" lg="4">
                    <FormGroup>
                      <Label for="parent">Parent</Label>
                      <Input type="text" name="parent" id="parent" />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="3" md="3" lg="3">
                    <FormGroup>
                      <Label for="address">ที่อยู่</Label>
                      <Input type="text" name="address" id="address" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="3" md="3" lg="3">
                    <FormGroup>
                      <Label for="village">หมู่บ้าน</Label>
                      <Input type="text" name="village" id="village" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="3" md="3" lg="3">
                    <FormGroup>
                      <Label for="villageNo">หมู่</Label>
                      <Input type="text" name="villageNo" id="villageNo" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="3" md="3" lg="3">
                    <FormGroup>
                      <Label for="Lane">ซอย</Label>
                      <Input type="text" name="Lane" id="Lane" />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="6" md="4" lg="4">
                    <FormGroup>
                      <Label for="street">ถนน</Label>
                      <Input type="text" name="street" id="street" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="4" lg="4">
                    <FormGroup>
                      <Label for="subArea">แขวง</Label>
                      <Input type="text" name="subArea" id="subArea" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="4" lg="4">
                    <FormGroup>
                      <Label for="district">เขต</Label>
                      <Input type="text" name="district" id="district" />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label for="provice">จังหวัด</Label>
                      <Input type="text" name="provice" id="provice" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label for="postalCode">รหัสไปรษณีย์</Label>
                      <Input type="text" name="postalCode" id="postalCode" />
                    </FormGroup>
                  </Col>
                </Row>
              </Form>

              <Form>
                <h6>2. ข้อมูลผู้ติดต่อ</h6>
                <Row>
                  <Col xs="12" sm="6" md="4" lg="4">
                    <FormGroup>
                      <Label for="contact">Contact Name</Label>
                      <Input type="text" name="contact" id="contact" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="4" lg="4">
                    <FormGroup>
                      <Label for="tel">Phone No.</Label>
                      <Input type="text" name="tel" id="tel" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="4" lg="4">
                    <FormGroup>
                      <Label for="email">Email</Label>
                      <Input type="text" name="email" id="email" />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="6" md="4" lg="4">
                    <FormGroup>
                      <Label for="type">Type</Label>
                      <Input type="text" name="type" id="type" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="4" lg="4">
                    <FormGroup>
                      <Label for="cusProduct">Customer Product</Label>
                      <Input type="text" name="cusProduct" id="cusProduct" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="4" lg="4" />
                </Row>
                <Row>
                  <Col xs="12" sm="6" md="4" lg="4">
                    <FormGroup>
                      <Label for="contact2">Contact Name 2</Label>
                      <Input type="text" name="contact2" id="contact2" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="4" lg="4">
                    <FormGroup>
                      <Label for="tel2">Phone No. 2</Label>
                      <Input type="text" name="tel2" id="tel2" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="4" lg="4">
                    <FormGroup>
                      <Label for="email2">Email 2</Label>
                      <Input  type="text" name="email2" id="email2" />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="6" md="4" lg="4">
                    <FormGroup>
                      <Label for="type2">Type</Label>
                      <Input type="text" name="type2" id="type2" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="4" lg="4">
                    <FormGroup>
                      <Label for="cusProduct">Customer Product</Label>
                      <Input type="text" name="cusProduct" id="cusProduct" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="4" lg="4">
                    <FormGroup>
                      <Label for="grade">Grade</Label>
                      <Input type="text" name="grade" id="grade" />
                    </FormGroup>
                  </Col>
                </Row>
              </Form>

              <Form>
                <h6>3. ข้อมูลฝ่ายขาย</h6>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup className="mt-3">
                      <FormControl fullWidth className={styles.formControl}>
                        <InputLabel htmlFor="demo-controlled-open-select">
                          Sale name
                        </InputLabel>
                        <Select
                          value={this.state.selectsale}
                          onChange={this.handleSalename}
                          inputProps={{
                            name: "selectsale",
                            id: "selectsale"
                          }}
                        >
                          <MenuItem key={-1} value={"none"}>
                            {"ไม่มี Sale"}
                          </MenuItem>
                          {this.state.sales}
                        </Select>
                      </FormControl>
                    </FormGroup>
                    <FormGroup style={{ display: "none" }}>
                      <Label for="telSale">Sale name</Label>
                      <Input type="text" name="salename" id="salename" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label for="telSale">Phone No. (Sales)</Label>
                      <Input type="text" name="telSale" value={this.state.tel} id="telSale" disabled />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label for="emailSale">Email (Sales)</Label>
                      <Input
                        type="text"
                        value={this.state.emailSale}
                        name="emailSale"
                        id="emailSale"
                        disabled
                      />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup className="mt-3">
                      <FormControl fullWidth className={styles.formControl}>
                        <InputLabel htmlFor="demo-controlled-open-select">
                          Call Center Name
                        </InputLabel>
                        <Select
                          value={this.state.selectCallcenter}
                          onChange={this.handleCallcentername}
                          inputProps={{
                            name: "selectCallcenter",
                            id: "selectCallcenter"
                          }}
                        >
                          <MenuItem key={-1} value={"none"}>
                            {"ไม่มี Call Center"}
                          </MenuItem>
                          {this.state.callcenter}
                        </Select>
                      </FormControl>
                    </FormGroup>
                  </Col>
                </Row>
              </Form>

              <Form>
                <h6>4. ข้อมูลทางบัญชี</h6>
                <Row>
                  <Col xs="12" sm="4" md="4" lg="4">
                    <FormGroup>
                      <Label for="saleName">Financial Grade</Label>
                      <Input type="text" name="saleName" id="saleName" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="4" md="4" lg="4">
                    <FormGroup>
                      <Label for="telSale">Billing Name</Label>
                      <Input type="text" name="telSale" id="telSale" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="4" md="4" lg="4">
                    <FormGroup>
                      <Label for="telSale">Billing Place</Label>
                      <Input type="text" name="telSale" id="telSale" />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="4" md="4" lg="4">
                    <FormGroup>
                      <Label for="receiveDate">Cheque Receive Date</Label>
                      <Input type="date" name="receiveDate" id="receiveDate" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="4" md="4" lg="4">
                    <FormGroup>
                      <Label for="createDate">Bill Create Date</Label>
                      <Input type="date" name="createDate" id="createDate" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="4" md="4" lg="4">
                    <FormGroup>
                      <Label for="officerName">Finance Officer Name</Label>
                      <Input type="text" name="officerName" id="officerName" />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label for="tel">Phone No.</Label>
                      <Input type="text" name="tel" id="tel" />
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6" md="6" lg="6">
                    <FormGroup>
                      <Label for="email">Email</Label>
                      <Input type="text" name="email" id="email" />
                    </FormGroup>
                  </Col>
                </Row>
                <div className="text-right">
                  <Button
                    color="success"
                    style={{ color: "white", padding: "10px" }}
                  >
                    <Icon className="pt-1">save</Icon> บันทึก{" "}
                  </Button>{" "}
                </div>
              </Form>
            </Col>
          </Row>
        </Card>
      </div>
    );
  }
}
