import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Table from "components/Table/Table.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import MaterialTable from "material-table";
import CardBody from "components/Card/CardBody.jsx";

import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";
import "../Dashboard/dashboard.css";

class ApproveUser extends React.Component {
  state = {
    value: 0
  };
  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };
  render() {
    const { classes } = this.props;
    return (
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <Card>
              <CardHeader color="info">
                <h4 className={classes.cardTitleWhite}>
                  รายการอนุมัติบัญชีผู้ใช้งาน
                </h4>
                <p className={classes.cardCategoryWhite}>
                  ตารางแสดงรายการอนุมัติสิทธิของบัญชีผู้ใช้งานต่างๆ
                </p>
              </CardHeader>
              <CardBody>
                <MaterialTable
                  title="ประวัติการเปลี่ยนแปลง"
                  columns={[
                    {
                      title: "สถานะ",
                      field: "status",
                      defaultSort: "desc"
                    },
                    {
                      title: "กรอกโดย",
                      field: "create_by"
                    },
                    {
                      title: "รายละเอียด",
                      render: rowdata => {
                        return (
                          <div
                            dangerouslySetInnerHTML={{
                              __html: rowdata.text
                            }}
                          />
                        );
                      }
                    },
                    {
                      title: "เอกสาร",
                      render: rowdata => {
                        return (
                          <div
                            dangerouslySetInnerHTML={{
                              __html: rowdata.doc
                            }}
                          />
                        );
                      }
                    },
                    { title: "วันที่", field: "create_date" }
                  ]}
                  data={this.state.notes}
                />
              </CardBody>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

ApproveUser.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(ApproveUser);
