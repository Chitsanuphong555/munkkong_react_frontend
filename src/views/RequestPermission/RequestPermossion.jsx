import React from "react";
import PropTypes from "prop-types";
// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";

import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";
import "../Dashboard/dashboard.css";

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    let data = JSON.parse(localStorage.getItem("data"));
    let status = "";

    if (data.acstatus == "1") status = "Admin CallCenter";
    if (data.acstatus == "2") status = "CallCenter";
    if (data.acstatus == "3") status = "Sale";
    if (data.acstatus == "4") status = "Admin Sale";
    if (data.acstatus == "10") status = "Super Admin";
    this.state = {
      selectsale: "",
      status: status
    };
  }
  handleSimple = event => {
    this.setState({ [event.target.name]: event.target.value });
  };
  render() {
    const { classes } = this.props;
    return (
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <Card>
              <CardHeader color="warning">
                <h4 className={classes.cardTitleWhite}>เลือกสิทธิ์ผู้ใช้งาน</h4>
                <b>[ปัจจุบันสิทธิ์ของคุณคือ {this.state.status}]</b>
              </CardHeader>
              <CardBody>
                <div className="mt-4" />
                <FormControl fullWidth className={classes.selectFormControl}>
                  <InputLabel
                    htmlFor="simple-select"
                    className={classes.selectLabel}
                  >
                    ตำแหน่ง
                  </InputLabel>
                  <Select
                    MenuProps={{
                      className: classes.selectMenu
                    }}
                    classes={{
                      select: classes.select
                    }}
                    value={this.state.selectsale}
                    onChange={this.handleSimple}
                    inputProps={{
                      name: "selectsale",
                      id: "selectsale"
                    }}
                  >
                    <MenuItem
                      classes={{
                        root: classes.selectMenuItem,
                        selected: classes.selectMenuItemSelected
                      }}
                      value="1"
                    >
                      Call Center
                    </MenuItem>
                    <MenuItem
                      classes={{
                        root: classes.selectMenuItem,
                        selected: classes.selectMenuItemSelected
                      }}
                      value="2"
                    >
                      Admin Call Center
                    </MenuItem>
                    <MenuItem
                      classes={{
                        root: classes.selectMenuItem,
                        selected: classes.selectMenuItemSelected
                      }}
                      value="3"
                    >
                      Sale
                    </MenuItem>
                    <MenuItem
                      classes={{
                        root: classes.selectMenuItem,
                        selected: classes.selectMenuItemSelected
                      }}
                      value="4"
                    >
                      Admin Sale
                    </MenuItem>
                  </Select>
                </FormControl>
              </CardBody>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Dashboard);
